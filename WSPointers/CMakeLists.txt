



file(GLOB HEADERS WSPointers/*.h)

ROOT_GENERATE_DICTIONARY(G__WSPointers ${HEADERS} LINKDEF src/LinkDef.h)

file(GLOB SOURCES src/*.cxx)

add_library(WSPointers SHARED ${SOURCES} G__WSPointers )
target_link_libraries(WSPointers ${ROOT_LIBRARIES})

target_sources(WSPointers PRIVATE ${CMAKE_BINARY_DIR}/versioning/WSPointersVersion.h)
set_source_files_properties(${CMAKE_BINARY_DIR}/versioning/WSPointersVersion.h PROPERTIES GENERATED TRUE)
target_include_directories(WSPointers PRIVATE ${CMAKE_BINARY_DIR}/versioning)
target_include_directories(WSPointers PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
add_dependencies(WSPointers WSPointers_version)


enable_testing()
include(GoogleTest)
add_executable(TestWSPointers test/TestWSPointers.cxx)
target_link_libraries(TestWSPointers WSPointers gtest gtest_main)

install( DIRECTORY WSPointers DESTINATION include FILES_MATCHING
        COMPONENT headers
        PATTERN "*.h"
        )

install( FILES ${CMAKE_CURRENT_BINARY_DIR}/libWSPointers.rootmap
        ${CMAKE_CURRENT_BINARY_DIR}/libWSPointers_rdict.pcm
        DESTINATION lib
        COMPONENT libraries)

install(TARGETS WSPointers
        LIBRARY DESTINATION lib
        COMPONENT libraries)