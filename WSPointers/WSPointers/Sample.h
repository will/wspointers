

class Sample : public Component {

public:
    RooAbsReal* get() const;
    RooAbsReal* operator->() const { return get(); }
    RooAbsReal* coef() const override; // sample coefficient if sample is "in" a channel



};