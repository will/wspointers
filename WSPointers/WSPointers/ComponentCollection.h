

#include "Component.h"

/**
 * A container for a collection of components, iterable as a map, accessible as a vector
 */
class ComponentCollection : public std::map<std::string, Component> {

public:
    //using std::map<std::string, Component>::operator[];
    using std::map<std::string, Component>::at;
    Component& operator[](int i) { return at(m_keys.at(i)); }
    Component& operator[](const std::string& i) { return std::map<std::string, Component>::operator[](i); } // pyROOT wants this explicitly
    Component& at(int i) { return operator[](i); }
    void push_back(std::pair<const std::string, Component>& c ) { m_keys.push_back(c.first); std::map<std::string, Component>::operator[](c.first) = c.second; }
    void push_back(const std::string& key, const Component&& c) { m_keys.push_back(key); std::map<std::string, Component>::operator[](key) = c; }
    void push_back(const Component&& c) { push_back(c.GetName(),std::move(c)); }
    void push_back(const Component& c) { push_back(c.GetName(),std::move(c)); }
    std::vector<Component> vec() { std::vector<Component> out; for(auto& k : m_keys) { out.push_back(at(k));} return out; }
    Component& back() { return operator[](m_keys.size()-1); }

    void Print(Option_t* opt="") {
        for(auto& k : m_keys) {
            std::cout << k << " => "; std::map<std::string, Component>::at(k).Print(opt);
        }
    }

private:
    std::vector<std::string> m_keys; // the key order

};