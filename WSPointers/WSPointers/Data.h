#pragma once

class RooAbsData;
class RooArgSet;
class RooWorkspace;

#include "TNamed.h"
#include "Component.h"

class Data : public TNamed {
public:
    friend class Component;
    Data(const std::string& name, RooWorkspace* ws = nullptr);
    Data(RooAbsData* data, const RooArgSet* gobs = nullptr); // will take ownership

    void SetTitle(const char* title) override { if(auto o = get(); o) o->SetTitle(title); TNamed::SetTitle(title); }
    const char* GetTitle() const override { if(auto o = get(); o) return o->GetTitle(); return TNamed::GetTitle(); }

    Component comp(const std::string& name);

    RooAbsData* operator->() const { return get(); }
    RooAbsData* get() const;
    const RooArgSet* gobs() const;

    void Draw(Option_t* opt="same") override;

    double Integral() const;

    RooCategory* cat(const std::string& label) const; // return first category var containing this label

    Data channel(const std::string& channel) const;

    bool SetBinContent(int bin, double value);
    double GetBinContent(int bin) const;
    bool Add(TH1* hist);

    RooWorkspace* ws() const { return fWS; }

    Data reduce(const char* cut) const;

    std::shared_ptr<RooArgSet> fSnap; // values to use of observables and global observables
private:
    mutable std::shared_ptr<RooAbsData> fData;
    mutable std::shared_ptr<const RooArgSet> fGobs;
    RooWorkspace* fWS;


    mutable std::shared_ptr<TAxis> fXaxis;

};