
#pragma once

#define protected public
#include "RooAbsArg.h"
#undef protected

#define private public
#include "RooWorkspace.h"
#undef private

#include "RooAbsLValue.h"
#include "TH1D.h"
#include "RooAbsData.h"
#include "TGraphAsymmErrors.h"
#include "THStack.h"

#include "FitResult.h"
class Component;
#include "Data.h"

class Data;
class ComponentCollection;

#include "Fitting.h"

class TLegend;

class Component : public TNamed {

public:

    static RooWorkspace* currentSpace; // used when constructing new Components without specifying a space
    static RooWorkspace& space(); // returns the current workspace

    static void DividePad(const std::string& channels, int ncols=0);

    // idea: workspace could be replaced with generic 'owner', and parent can be replaced with another component?
    Component(const std::string& name="", RooWorkspace* ws = nullptr, const std::shared_ptr<Component>& parent = nullptr, const std::shared_ptr<Data>& = nullptr, const std::shared_ptr<RooAbsArg>& comp = nullptr);
    Component(const std::string& name, const Component& parent);
    Component(const std::shared_ptr<RooAbsArg>& comp, const Component& parent);
    Component(RooAbsArg& comp, const Component& parent); // will create a non-owning shared ptr of comp
    virtual ~Component() { }

    void SetTitle(const char* title) override { if(auto o = get(); o) o->SetTitle(title); else TNamed::SetTitle(title); }
    const char* GetTitle() const override { if(auto o = get(); o) return o->GetTitle(); return TNamed::GetTitle(); }

    Data generate(bool expected=false) const;
    RooAbsCategoryLValue* cat() const;
    Component reduce(const std::string& rangeName) const;

    std::map<std::string,Component> channels() const;
    std::vector<Component> samples() const;
    std::map<std::string,Component> constraints() const;
    Component constraint() const; // if this component has a constraint term, access it like this
    ComponentCollection factors() const; // samples can be factorized into parts. The first factor is the main factor (may be itself if no factors)
    ComponentCollection factors(int bin) const; // some bins can also be scaled individually
    ComponentCollection pars() const; // floating parameters of this component
    ComponentCollection args() const; // constant "parameters" of this component

    Component channel(const std::string& channelName) const;
    Component sample(const std::string& sampleTitle) const;

    Component varFactor(const std::string& factorName) const; // a variable factor (normFactor: RooRealVar or shapeFactor: ParamHistFunc)
    Component parFactor(const std::string& factorName) const; // a potentially parameterized factor (FlexibleInterpVar or RooHistFunc)

    Component factor(const std::string& factorName) const; // any type of scale factor

    std::shared_ptr<RooArgSet> globs(const FitResult& fr="") const;
    std::shared_ptr<RooArgSet> obs(const FitResult& fr="") const;
    RooAbsPdf* mainPdf() const; // in event that component is a RooProdPdf try to pick out the main pdf

    RooAbsArg* get() const;
    RooAbsArg* operator->() const { return get(); }

    virtual RooAbsReal* coef() const; // components that belong to another may have scaling terms that should be included in their content and error etc

    double GetBinContent(int bin, const FitResult& fr = "") const;
    double GetBinError(int bin, const FitResult& fr = "") const;
    double Integral(const FitResult& fr = "") const;
    TH1* BuildHistogram(const FitResult& opt = "");
    THStack* BuildStack(const FitResult& fr = "");

    TAxis* GetXaxis() const;

    bool SetBin(int bin) const; // move x axis var to this bin center

    RooWorkspace* ws() const { return fWS; }


    FitResult fitTo(const Data& _data, Fitting::FitConfig* fc = nullptr) const;
    double gofPValue(const Data& _data, size_t nToys=1000) const;
    double pll(RooArgSet& poi, const Data& _data,Fitting::FitConfig* fc = nullptr) const;

    void Draw(Option_t* opt="", const FitResult& fr = "");


    Component shallowCopy(const std::string& name);


    // passes acquisition onto parent, and when reaches top parent if its in a workspace
    // then will add it to the workspace otherwise adds it to fOwned and returns that
    std::shared_ptr<TObject> acquire(const std::shared_ptr<TObject>& arg);
    std::shared_ptr<TObject> getObject(const std::string& name) const;
    template<typename T> std::shared_ptr<T> getObject(const std::string& name) const { return std::dynamic_pointer_cast<T>(getObject(name)); }
    template<typename T> std::shared_ptr<T> getObject(const char* name) const { return std::dynamic_pointer_cast<T>(getObject(std::string(name))); }
    // common pattern for 'creating' an acquired object
    template<typename T, typename ...Args> std::shared_ptr<T> acquire(Args&& ...args) {
        return std::dynamic_pointer_cast<T>(acquire(std::make_shared<T>(std::forward<Args>(args)...)));
    }

    bool addConstraint(RooAbsPdf& pdf, bool allowDuplicate=false);
    RooAbsPdf* getConstraint(const RooAbsReal& par) const;
    RooAbsPdf* getConstraint() const;
    bool removeConstraint(const RooAbsPdf& pdf);
    void addGlobalObservable(RooAbsLValue& obs);
    void addParameter(RooAbsLValue& obs);

    // Idea: add SetBinData(dataName, bin, value), and FillData(dataName,value,weight)

    // Use to effectively create RooHistFuncs and PiecewiseInterpolations
    // In HistFactory terminology, this is the nominal histogram and HistoSys variations: lambda_csb
    bool SetContent(TH1* hist, const char* varPar=nullptr, bool high=true);
    // if bin=0, will create a FlexibleInterpVar that scales the sample -- aka an OverallSys
    bool SetBinContent(int bin, double value, const char* varPar=nullptr, bool high=true);
    // creates ShapeSys i.e. bin-by-bin uncorrelated scale factors
    bool SetBinError(int bin, double value, const char* varPar=nullptr, bool high=true);
    // Can be used to effectively add a NormFactor or a ShapeFactor (a ParamHistFunc)
    bool Scale(RooAbsReal& func);
    bool Scale(const std::string& funcName);
    bool Scale(const Component& comp);
    // Used to effectively add a part of a ShapeFactor
    bool Scale(int bin, RooAbsReal& func);

    bool Unscale(int bin, RooAbsReal& func);
    bool Unscale(const std::string& factorName);
    RooArgList scaleFactors(int bin) const;
    RooArgList scaleFactors() const;

    std::shared_ptr<RooRealVar> var(const std::string& name) const;
    Data data(const std::string& name) const;

    mutable std::shared_ptr<RooAbsArg> fComp; // when not owning a custom deleter will be used to avoid delete
    RooWorkspace* fWS;
    TString fParentName;
    std::shared_ptr<Component> fParent;
    std::vector<std::shared_ptr<TObject>> fOwned;
    std::shared_ptr<Data> fData;
    bool kIsFactor=false; // if true, when creating new histfuncs etc will flag as not a density
    bool kIsVarFactor=false; // if true, when creating new will create a ParamHistFunc and set of params
    bool kIsChannel=false; // used when creating

    void sanitizeWS();

    void sterilize();

    mutable std::shared_ptr<TAxis> fXaxis;

    static void profiling();

    static TLegend* getLegend(bool create=true);
    static void addLegendEntry(TObject* o, const char*, const char*);

public:
    ClassDefOverride(Component, 0); // A Smart pointer to a component of a RooFit computation graph
};