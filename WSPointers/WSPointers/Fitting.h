#pragma once

#include "RooAbsReal.h"
#include "Fit/FitConfig.h"

class Fitting {

public:
    struct FitConfig {
        std::string uuid;
        ROOT::Fit::FitConfig fitConfig;
        RooLinkedList nllOpts;
        TString rootVersion;
        bool returnBinnedNLL = false; // TODO make a bitmask, with RAW/CAT,TRUE/UNTRUE,BINNED/UNBINNED
    };

    static FitConfig& defaultConfig();


    static RooFitResult* fit(RooAbsPdf* pdf, RooAbsData* data, RooArgSet* gobs = nullptr, FitConfig* fitConfig = nullptr);

    static RooFitResult* minimize(RooAbsReal* func, FitConfig* fitConfig = nullptr);

    static TObject& msg() { if(m_msgObj==0) m_msgObj=new TObject; return *m_msgObj; }

    static TObject* m_msgObj;

private:
    static FitConfig* s_defaultConfig;

};