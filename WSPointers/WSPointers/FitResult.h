#pragma once

#include "TNamed.h"

class RooArgList;
class RooFitResult;

class Data;
class Component;

class FitResult : public TNamed {

public:
    friend class Component;
    FitResult(const std::string& opt);
    FitResult(const char* opt) : FitResult(std::string(opt)) { }
    FitResult(RooFitResult* fr, bool takeOwnership=false);

    RooFitResult* get() const { return fr.get(); }
    RooFitResult* operator->() const { return get(); }

    void reset(const RooArgList* pars); // creates a new RooFitResult with this being the list of floats

    void Draw(Option_t* opt = "") override;

    std::shared_ptr<Component> model() const { return fModel; }
    std::shared_ptr<Data> data() const { return fData; }

private:
    std::shared_ptr<RooFitResult> fr;

    std::shared_ptr<Data> fData;
    std::shared_ptr<Component> fModel;

};