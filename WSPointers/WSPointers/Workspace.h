#pragma once

#include "Component.h"
#include "Data.h"

#include "ComponentCollection.h"

class Workspace : public TNamed {

public:
    RooWorkspace* get() const;
    RooWorkspace* operator->() const { return get(); }

    Workspace(const std::string& nameOrFilename); // interpret as filename if contains ".root"


    ComponentCollection models() const; // finds top-level pdfs and assumes those are the models
    Component model(const std::string& name) const;

    std::vector<Data> datas() const; // yes I know data is already plural

    mutable std::shared_ptr<RooWorkspace> m_ws;

};