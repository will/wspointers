#pragma once

#include "Rtypes.h"
#include <limits>
#include "RooRealVar.h"

class Asymptotics {

public:

    enum PLLType {
        TwoSided = 0,
        OneSidedPositive, // for exclusions
        OneSidedNegative, // for discovery
        OneSidedAbsolute, // for exclusions by magnitude
        Uncapped, // for discovery with interest in deficits as well as excesses
        Unknown
    };

    // inverse of PValue function
    static Double_t k(PLLType compatFunc, double pValue, double poiVal, double poiPrimeVal,
            double sigma_mu=0,
            double low=-std::numeric_limits<double>::infinity(),
            double high=std::numeric_limits<double>::infinity());

    static Double_t PValue(PLLType _compatibilityFunction,
            double k,
            double mu,
            double mu_prime,
            double sigma_mu=0,
            double mu_low=-std::numeric_limits<double>::infinity(),
            double mu_high=std::numeric_limits<double>::infinity()
    );

    static Double_t PValue(PLLType _compatibilityFunction,
                           double k,
                           const RooRealVar& mu,
                           double mu_prime,
                           double sigma_mu=0
    ) {
        return PValue(_compatibilityFunction, k, mu.getVal(), mu_prime, sigma_mu, mu.getMin("physical"), mu.getMax("physical"));
    }

    static Double_t Phi_m(double mu, double mu_prime, double a, double sigma, PLLType _compatibilityFunction);


};