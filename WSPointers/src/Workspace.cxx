
#include "WSPointers/Workspace.h"
#include "TFile.h"
#include "TKey.h"

RooWorkspace* Workspace::get() const {
    if (m_ws) return m_ws.get();
    return nullptr;
}

Workspace::Workspace(const std::string& nameOrFilename) : TNamed(nameOrFilename.c_str(),"") {

    if( nameOrFilename.find(".root") != std::string::npos ) {
        TFile f(nameOrFilename.c_str());
        // look for a workspace
        auto keys = f.GetListOfKeys();
        if (keys) {
            for (auto &&k : *keys) {
                auto cl = TClass::GetClass(((TKey *) k)->GetClassName());
                if (cl == RooWorkspace::Class() || cl->InheritsFrom("RooWorkspace")) {
                    m_ws.reset( dynamic_cast<RooWorkspace *>(f.Get(k->GetName())) );
                    if (m_ws) break;
                }
            }
        }
        SetNameTitle("combined","combined");
    } else {
        m_ws.reset( dynamic_cast<RooWorkspace *>(gDirectory->Get(nameOrFilename.c_str())) );
    }

    if (!m_ws) {
        // create a new workspace
        m_ws.reset( new RooWorkspace(GetName(),GetTitle())  );
    }
    SetNameTitle(m_ws->GetName(),m_ws->GetTitle());

}

ComponentCollection Workspace::models() const {
    ComponentCollection out;
    auto ws = get();
    if (!ws) return out;
    for(auto& p : ws->allPdfs() ) {
        if (p->hasClients()) continue; // only top-level pdfs
        out.push_back( Component(p->GetName(),ws,nullptr,nullptr,std::shared_ptr<RooAbsArg>(p,[](RooAbsArg*){})) );
    }
    return out;
}

Component Workspace::model(const std::string& name) const {

    auto mods = models();
    if(auto s = mods.find(name) ; s != mods.end()) {
        return s->second;
    }
    return Component(name,get());
}