#include "WSPointers/Component.h"
#pragma link C++ class Component+;
#pragma link C++ class FitResult+;
#pragma link C++ class Data+;

#include "WSPointers/Workspace.h"
#pragma link C++ class Workspace+;