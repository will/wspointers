
#include "WSPointers/FitResult.h"

#define protected public
#include "RooFitResult.h"
#undef protected

#include "WSPointers/Component.h"

#include "TPRegexp.h"
#include "TRegexp.h"

#include "RooRealVar.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TGraphAsymmErrors.h"
#include "TLegend.h"
#include "TGraphErrors.h"
#include "TH2D.h"

void FitResult::reset(const RooArgList* pars) {
    if (!pars) { fr.reset(); }
    else {
        fr = std::make_shared<RooFitResult>();
        fr->setFinalParList(*pars);
    }
}

FitResult::FitResult(RooFitResult* fr, bool takeOwnership) :
    TNamed("",""), fr(fr,[takeOwnership](RooFitResult* fr){if(takeOwnership) delete fr;}) {}


FitResult::FitResult(const std::string& opt) : TNamed(opt,""), fr( new RooFitResult("")) {

    // see if can match string to any vars in the current workspace
    if (!opt.empty()) {
        auto vars = Component::space().allVars();

        RooArgList f;
        RooArgList c;
        std::vector<TRegexp> patterns;
        TStringToken pattern(opt, ";");
        while (pattern.NextToken()) {
            patterns.emplace_back(TRegexp(pattern, true));
        }
        for (auto v : vars) {
            bool added=false;
            TString n(v->GetName());
            if (v->getAttribute("Constant")) { c.add(*v); }
            else {
                for (auto p : patterns) {
                    if (n.Contains(p)) {
                        added=true;
                        f.add(*v);
                    }
                }
            }
            if (!added) c.add(*v);
        }
        //if (!f.empty()) {
            fr->setFinalParList(f);
            fr->setConstParList(c);
        //}
    }

}

void FitResult::Draw(Option_t* opt) {
    if (!fr) return;

    TString sOpt(opt);
    if(sOpt=="") sOpt += "pull"; //assume wanted pull

    if (sOpt.Contains("events")) {
        fModel->Draw("",*this);
        fData->Draw("same");
    } else if(sOpt.Contains("pull")) {
        //only show float pars that had initial errors
        std::vector<int> indices;

        double pullThresh = 0;
        /*if(opt.Contains("threshold=")) {
            //going to filter variables so only ones with at least one non-diagonal entry with magnitude greater than threshold are shown ...
            pullThresh = extractNumber(opt,"threshold=");
        }*/


        for(int i=0;i<fr->_finalPars->getSize();i++) {
            RooRealVar* rinit = static_cast<RooRealVar*>(fr->_initPars->at(i));
            //if(args.getSize()&&!args.find(*rinit)) continue; //if args are specified, then must be in the list
            if(rinit->getError()) {
                if(pullThresh) {
                    RooRealVar* r = static_cast<RooRealVar*>(fr->_finalPars->at(i));
                    if( fabs((r->getVal()-rinit->getVal())/rinit->getError()) < pullThresh) continue;
                }
                indices.push_back(i);continue;
            }
            // TODO: infer initial error if none given
        }
        if(indices.size()) {
            int xOffset = 0;
            TH1* fPullFrame;
            TLegend* fLegend = nullptr;
            bool madeLegend = false;
            if (!sOpt.Contains("same")) {
                fPullFrame = new TH1D("frame",GetTitle(),indices.size(),0,indices.size());fPullFrame->SetLineColor(0);fPullFrame->SetDirectory(0);
                fPullFrame->SetStats(0);
                for(uint i=0;i<indices.size();i++) {
                    fPullFrame->GetXaxis()->SetBinLabel(i+1,fr->floatParsFinal()[indices[i]].GetTitle());
                }
                fPullFrame->GetXaxis()->LabelsOption("v");
                fPullFrame->SetMaximum(3);fPullFrame->SetMinimum(-3);
                fPullFrame->GetYaxis()->SetTitle("(#hat{#theta}-#theta_{i})/#sigma_{i}");

                fPullFrame->Draw();
                TGraphErrors* g2 = new TGraphErrors(2);g2->SetPoint(0,0,0);g2->SetPointError(0,0,2);g2->SetName("2sigma");
                g2->SetBit(kCanDelete);
                g2->SetPoint(1,fPullFrame->GetXaxis()->GetXmax(),0);g2->SetPointError(1,1,2);g2->SetFillColor(kYellow);
                g2->Draw("3");
                g2 = new TGraphErrors(2);g2->SetPoint(0,0,0);g2->SetPointError(0,0,1);g2->SetName("1sigma");
                g2->SetBit(kCanDelete);
                g2->SetPoint(1,fPullFrame->GetXaxis()->GetXmax(),0);g2->SetPointError(1,1,1);g2->SetFillColor(kGreen);
                g2->Draw("3");
                TGraph* g = new TGraph(2);g->SetPoint(0,0,0);g->SetName("midline");
                g->SetLineStyle(2);
                g->SetBit(kCanDelete);
                g->SetPoint(1,fPullFrame->GetXaxis()->GetXmax(),0);
                g->Draw("L");

                fPullFrame->Draw("sameaxis");

            } else {
                // get the frame from the pad
                if (!gPad) {
                    TCanvas::MakeDefCanvas();
                }
                fPullFrame = dynamic_cast<TH1*>(gPad->GetPrimitive("frame"));
                if (!fPullFrame) return;

                for(uint i=0;i<indices.size();i++) {
                    fPullFrame->GetXaxis()->FindBin( fr->floatParsFinal()[indices[i]].GetTitle() ); //automatically creates a new bin if not found
                }
                fPullFrame->LabelsDeflate(); //removes any unused bins

                fLegend = dynamic_cast<TLegend*>(gPad->GetPrimitive("legend"));

                if (!fLegend) {
                    fLegend = new TLegend(0.7,0.7,1. - gPad->GetRightMargin()-0.05,1.-gPad->GetTopMargin()-0.05);
                    fLegend->Draw();
                    madeLegend = true;
                }

                // count how many results already displayed, use to set xOffset
                for(auto o : *gPad->GetListOfPrimitives()) {
                    if(o->InheritsFrom(TGraphAsymmErrors::Class())) {
                        if(xOffset<=0) xOffset = 1 - xOffset;
                        else xOffset = -xOffset;
                        if (madeLegend) {
                            fLegend->AddEntry(o,o->GetTitle(),"l");
                        }
                    } else if(strcmp(o->GetName(),"midline")==0 || strcmp(o->GetName(),"1sigma") || strcmp(o->GetName(),"2sigma")) {
                        dynamic_cast<TGraph*>(o)->SetPoint(1,fPullFrame->GetXaxis()->GetXmax(),0);
                    }
                }


            }

            TGraphAsymmErrors* g = new TGraphAsymmErrors;
            g->SetBit(kCanDelete);
            TGraphAsymmErrors& fPullGraph = *g;
            fPullGraph.Set(0);fPullGraph.SetMarkerStyle(20);
            for(uint i=0;i<indices.size();i++) {
                RooRealVar* r = static_cast<RooRealVar*>(fr->_finalPars->at(indices[i]));
                RooRealVar* rinit = static_cast<RooRealVar*>(fr->_initPars->at(indices[i]));
                double pull = (r->getVal()-rinit->getVal())/rinit->getError();
                double pullUp = (r->getVal()+r->getErrorHi()-rinit->getVal())/rinit->getError();
                double pullDown = (r->getVal()+r->getErrorLo()-rinit->getVal())/rinit->getError();

                //find bin using labels ...
                int binNum = fPullFrame->GetXaxis()->FindBin( r->GetTitle() );

                fPullGraph.SetPoint(fPullGraph.GetN(),binNum-0.5+0.2*xOffset,pull);
                fPullGraph.SetPointError(fPullGraph.GetN()-1,0,0,pull-pullDown,pullUp-pull);
                if(pullDown < fPullFrame->GetMinimum()) fPullFrame->SetMinimum(pullDown-1);
                if(pullUp > fPullFrame->GetMaximum()) fPullFrame->SetMaximum(pullUp+1);
            }


            fPullGraph.Draw("p");

            if(fLegend) fLegend->AddEntry(&fPullGraph,GetTitle(),"l");


        } else {
            Warning("Draw","No parameters selected");
        }
    } else if(sOpt.Contains("cov") || sOpt.Contains("cor")) {
        const RooArgList* a = &get()->floatParsFinal();
        auto covHist = new TH2D(sOpt.Contains("cov") ? "covariance" : "correlation",get()->GetTitle(),a->getSize(),0,a->getSize(),a->getSize(),0,a->getSize());
        covHist->SetDirectory(0);

        auto cov = get()->reducedCovarianceMatrix(*a);

        for(int i=1;i<=a->getSize();i++) {
            covHist->GetXaxis()->SetBinLabel(i, a->at(i-1)->GetTitle());
            for(int j=1;j<=a->getSize();j++) {
                if(i==1) covHist->GetYaxis()->SetBinLabel(j, a->at(j-1)->GetTitle());
                if(sOpt.Contains("cor")) {
                    covHist->SetBinContent(i,j,cov(i-1,j-1)/sqrt(cov(i-1,i-1)*cov(j-1,j-1)));
                } else {
                    covHist->SetBinContent(i,j,cov(i-1,j-1));
                }
            }
        }
        if(sOpt.Contains("cor")) {
            covHist->SetAxisRange(-1,1,"Z");
            covHist->GetZaxis()->SetTitle("Correlation");
        } else {
            covHist->GetZaxis()->SetTitle("Covariance");
        }
        covHist->GetXaxis()->LabelsOption("v");
        covHist->SetStats(0);
        covHist->SetBit(kCanDelete);
        if(sOpt.Contains("text")) covHist->Draw("COLZ TEXT");
        else covHist->Draw("COLZ");
        gPad->SetRightMargin( std::max(gPad->GetRightMargin(),(float)0.2) );
    }
}