
#include <memory>

#include "WSPointers/Component.h"
#include "WSPointers/Fitting.h"
#include "WSPointers/ComponentCollection.h"
#include "TRegexp.h"

#define private public
#define protected public
#include "RooRealSumPdf.h"
#include "RooProdPdf.h"
#include "RooProduct.h"
#include "RooHistFunc.h"
#include "RooFitResult.h"
#include "RooStats/HistFactory/ParamHistFunc.h"
#include "RooStats/HistFactory/PiecewiseInterpolation.h"
#include "RooStats/HistFactory/FlexibleInterpVar.h"
#include "RooPoisson.h"
#include "RooGamma.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#undef protected
#undef private

#include "TKey.h"

#include "RooSimultaneous.h"
#include "RooRealVar.h"
#include "TLegend.h"
#include "TPRegexp.h"
#include "TLatex.h"
#include "THistPainter.h"
#include "TPad.h"
#include "TCanvas.h"

#include "RooCategory.h"

#include "RooDataSet.h"
#include "RooLognormal.h"
#include "RooBifurGauss.h"

#include "RooMsgService.h"
#include "RooBinning.h"
#include "RooUniformBinning.h"

#include "TStopwatch.h"
class ProfileClock {
public:
    ProfileClock(const std::string& name) {
        fName = name;
        fClocks[name].Start(false);
    }
    TStopwatch& get() { return fClocks[fName]; }
    ~ProfileClock() { fClocks[fName].Stop(); }
    static std::map<std::string,TStopwatch> fClocks;
    std::string fName;
};

std::map<std::string,TStopwatch> ProfileClock::fClocks;

void Component::profiling() {
    for(auto& [k,v] : ProfileClock::fClocks) {
        std::cout << k << " : " ; v.Print();
        v.Reset();
    }
}

void Component::sanitizeWS() {
    std::set<std::string> setNames;
    for(auto& a : ws()->_namedSets) {
        if (TString(a.first.c_str()).BeginsWith("CACHE_")) { setNames.insert(a.first); }
    }
    for(auto& a : setNames) ws()->removeSet(a.c_str());
}

RooWorkspace* Component::currentSpace = nullptr;

RooWorkspace& Component::space() {
    if (currentSpace) { return *currentSpace; }
    RooMsgService::instance().getStream(RooFit::INFO).removeTopic(RooFit::NumIntegration); //stop info message every time

    if (gDirectory) {
        // try to find a workspace in the current directory
        auto keys = gDirectory->GetListOfKeys();
        if (keys) {
            for (auto &&k : *keys) {
                auto cl = TClass::GetClass(((TKey *) k)->GetClassName());
                if (cl == RooWorkspace::Class() || cl->InheritsFrom("RooWorkspace")) {
                    currentSpace = dynamic_cast<RooWorkspace *>(gDirectory->Get(k->GetName()));
                    if (currentSpace) return *currentSpace;
                }
            }
        }
    }
    currentSpace = new RooWorkspace("combined");
    return *currentSpace;
}

class Axis : public TAxis {

public:
    using TAxis::TAxis;
    Double_t GetBinWidth(Int_t bin) const override { if(auto v = var(); v)return v->getBinWidth(bin-1,GetName()); return 1; }
    Double_t GetBinLowEdge(Int_t bin) const override { if(auto v = rvar(); v)return v->getBinning(GetName()).binLow(bin-1); return bin-1; }
    Double_t GetBinUpEdge(Int_t bin) const override { if(auto v = rvar(); v)return v->getBinning(GetName()).binHigh(bin-1); return bin; }

    const char* GetTitle() const override { return (binning()) ? binning()->GetTitle() : GetParent()->GetTitle();  }
    void SetTitle(const char* title) override { if(binning()) const_cast<RooAbsBinning*>(binning())->SetTitle(title); else dynamic_cast<TNamed*>(GetParent())->SetTitle(title); }

    const RooAbsBinning* binning() const { return var()->getBinningPtr(GetName()); }

    Int_t 	FindFixBin (Double_t x) const override { return (binning()) ? (binning()->binNumber(x)+1) : x; }

private:
    RooAbsLValue* var() const { return dynamic_cast<RooAbsLValue*>(GetParent()); }
    RooAbsRealLValue* rvar() const { return dynamic_cast<RooAbsRealLValue*>(GetParent()); }

};
/*
Component::Component(const std::string& name, RooWorkspace* ws, const Component* parent) :
    TNamed(name, ""), fWS( (ws) ? ws : &space() ), fParent((parent) ? new Component(*parent) : nullptr) {
}
 */
Component::Component(const std::string& name, RooWorkspace* ws, const std::shared_ptr<Component>& parent, const std::shared_ptr<Data>& data, const std::shared_ptr<RooAbsArg>& comp) :
        TNamed(name, ""), fWS( (ws) ? ws : &space() ), fParent(parent), fData(data), fComp(comp) {
}
Component::Component(const std::string& name, const Component& parent) : Component(name,parent.ws(),std::make_shared<Component>(parent),parent.fData) { }

Component::Component(const std::shared_ptr<RooAbsArg>& comp, const Component& parent) : Component((comp) ? comp->GetName() : "", parent) {
    fComp = comp;
}

Component::Component(RooAbsArg& comp, const Component& parent) : Component(std::shared_ptr<RooAbsArg>(&comp,[](RooAbsArg*){}),parent) { }

RooAbsCategoryLValue* Component::cat() const {
    if(auto simPdf = dynamic_cast<RooSimultaneous*>(get()); simPdf) {
        return &const_cast<RooAbsCategoryLValue&>(simPdf->indexCat());
    }
    return nullptr;
}

Component Component::reduce(const std::string& rangeName) const {

    auto _cat = cat();
    if (!_cat) { return Component("",fWS); }

    std::vector<TRegexp> chanPatterns;
    TStringToken pattern(rangeName, ",");
    while (pattern.NextToken()) {
        chanPatterns.emplace_back(TRegexp(pattern, true));
    }
    auto newPdf = std::make_shared<RooSimultaneous>(TString::Format("%s_reduced",GetName()),"Reduced model",*_cat);
    for(auto& [c,p] : channels()) {
        _cat->setLabel(c.c_str());
        bool matchAny=false;
        for(auto& p : chanPatterns) {
            if (TString(c).Contains(p)) { matchAny=true; break; }
        }
        if ((_cat->hasRange(rangeName.c_str()) && _cat->inRange(rangeName.c_str())) || (!_cat->hasRange(rangeName.c_str()) && matchAny)) {
            newPdf->addPdf( *dynamic_cast<RooAbsPdf*>(p.get()), c.c_str() );
        }
    }
    return Component(newPdf->GetName(),fWS,nullptr,fData,newPdf);
}

#include "Math/SpecFuncMathCore.h"

FitResult Component::fitTo(const Data& _data, Fitting::FitConfig* fc) const {
    auto _pdf = dynamic_cast<RooAbsPdf*>(get());
    if (!_pdf) return FitResult((RooFitResult*)nullptr);
    if (!_data.get()) return FitResult((RooFitResult*)nullptr);

    if (!fc) fc = &Fitting::defaultConfig();

    int printLevel  =   fc->fitConfig.MinimizerOptions().PrintLevel();
    RooFit::MsgLevel msglevel = RooMsgService::instance().globalKillBelow();
    if(printLevel < 0) RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);

    auto nllOpts = fc->nllOpts;
    TObject* obj = nullptr;
    if(_data.gobs()) nllOpts.Add(obj = RooFit::GlobalObservables(*_data.gobs()).Clone("GlobalObservables"));
    auto _nll = _pdf->createNLL( *_data.get(), nllOpts);
    if (obj) delete obj;

    if(printLevel < 0) RooMsgService::instance().setGlobalKillBelow(msglevel);

    // move gobs to their values
    if(_data.gobs()) *std::shared_ptr<RooArgSet>( _pdf->getObservables(*_data.gobs()) ) = *_data.gobs();
    auto out = Fitting::minimize(_nll, fc);
    delete _nll;

    // if was RooSimultaneous extended fit, convert to uNLL with:
    // uNLL = rawNLL - Nlog(nCat)
    // to convert uNLL (unbinned NLL) to the true uNLL would need to add:
    // true_uNLL = uNLL + log(N!)
    // To convert a true_uNLL to a true_bNLL:
    // true_bNLL = true_uNLL - log(N!) + Sum(log(n!))
    // so true_bNLL = uNLL + Sum(log(n!))
    double N = 0;
    double sum_log_ni = 0;
    double binWidthFactor = 0;
    auto chans = channels();
    if (!chans.empty()) {
        for(auto& c : chans) {
            auto _chan_data = _data.channel(c.first);
            auto chanN = _chan_data.Integral();
            N += chanN;
            if (chanN && _chan_data->isWeighted()) { // should really be summing up entries in each bin but we will assume there's only one for each
                auto _cat = _chan_data.cat(c.first);
                for (int i = 0; i < _chan_data->numEntries(); i++) {
                    _data->get(i);
                    // actually need to check the category matches our channel, because dataset is NOT a subset
                    if (_cat && c.first!=_cat->getLabel()) continue;
                    sum_log_ni += ROOT::Math::lgamma(_data->weight() + 1); // = log(n!)
                    binWidthFactor += _data->weight()*log( c.second.GetXaxis()->GetBinWidth(c.second.GetXaxis()->FindFixBin(_data->get(i)->getRealValue(c.second.GetXaxis()->GetParent()->GetName()))));
                }
            }
        }
        out->setMinNLL(out->minNll() - N*log(chans.size()));
    } else {
        N = _data.Integral();
        const RooCategory* catvar = (strlen(_data.TNamed::GetTitle())) ? _data.cat(_data.TNamed::GetTitle()) : nullptr;
        if (_data->isWeighted()) { // should really be summing up entries in each bin but we will assume there's only one for each
            for (int i = 0; i < _data->numEntries(); i++) {
                _data->get(i);
                if (catvar && strcmp(catvar->getLabel(),_data.TNamed::GetTitle())) continue;
                sum_log_ni += ROOT::Math::lgamma(_data->weight() + 1); // = log(n!)
                // need to also get the bin width of this event ....
                // because need to subtract weight*log(binWidth) to account for
                // probability in unbinned mode being log(density) instead of log(prob)
                binWidthFactor += _data->weight()*log( GetXaxis()->GetBinWidth(GetXaxis()->FindFixBin(_data->get(i)->getRealValue(GetXaxis()->GetParent()->GetName()))));
            }
        }
    }

    if (fc->returnBinnedNLL) {
        out->setMinNLL(out->minNll() + sum_log_ni - binWidthFactor);
    }

    FitResult fr((RooFitResult*)out,true);
    fr.SetName(TUUID().AsString());
    fr.fData = std::make_shared<Data>( _data );
    fr.fModel = std::make_shared<Component>( *this );
    fr.SetTitle(Form("%s fit to %s",strlen(GetTitle())?GetTitle(),GetName(),strlen(_data.GetTitle())?_data.GetTitle():_data.GetName()));

    return fr;

}

double Component::pll(RooArgSet& poi, const Data& _data,Fitting::FitConfig* fc) const {
    if (!get()) return std::numeric_limits<double>::quiet_NaN();
    double sf = 1.;
    auto snap = std::unique_ptr<RooArgSet>(poi.snapshot());
    auto allPars = std::unique_ptr<RooArgSet>(get()->getParameters(_data.get()));
    auto pSnap = std::unique_ptr<RooArgSet>(allPars->snapshot());
    poi.setAttribAll("Constant",false);
    auto unconditionalFit = fitTo(_data,fc);
    poi = *snap;
    poi.setAttribAll("Constant",true);
    auto conditionalFit = fitTo(_data,fc);
    *allPars = *pSnap;
    return sf*2.0*(conditionalFit->minNll()+conditionalFit->edm() - (unconditionalFit->minNll() - unconditionalFit->edm()));
}


double Component::gofPValue(const Data& _data, size_t nToys) const {
    auto fc = new Fitting::FitConfig(Fitting::defaultConfig());
    double out = 0;

    fc->fitConfig.MinimizerOptions().SetMaxFunctionCalls(0); // so just evaluates nll
    fc->returnBinnedNLL=true;
    fc->fitConfig.MinimizerOptions().SetPrintLevel(-1);

    double obs = fitTo(_data,fc)->minNll();

    Info("gobsPValue","Observed nll: %f",obs);

    for(size_t i = 0;i<nToys;i++) {
        out += (fitTo(generate(),fc)->minNll() >= obs);
    }

    delete fc;
    return out/nToys;
}

RooAbsPdf* Component::mainPdf() const {
    // note: could be smarter here and look for pdf that depends on the obs
    auto o = dynamic_cast<RooAbsPdf*>(get());
    if (!o) return nullptr;
    if (auto p = dynamic_cast<RooProdPdf*>(o); p) {
        for(auto l : p->pdfList()) {
            if (l->getAttribute("MAIN_MEASUREMENT") || dynamic_cast<RooRealSumPdf*>(l) /*|| dynamic_cast<RooAddPdf*>(l)*/) {
                return dynamic_cast<RooAbsPdf*>( l );
            }
        }
        //Error("mainPdf","Could not determine main pdf of %s",GetName());
        return nullptr;
    } else {
        return dynamic_cast<RooAbsPdf*>( o );
    }
}

Data Component::generate(bool expected) const {
    Data out(nullptr,nullptr);

    auto _pdf = dynamic_cast<RooAbsPdf*>(get());
    if (!_pdf) { return out; }

    //if (!fData) { return out; }

    RooArgSet* _gobs = nullptr;

    auto _g = globs();

    if (_g) {
        _gobs = dynamic_cast<RooArgSet*>(_g->selectByAttrib("Constant",true)); // only use the constant parameters as the gobs
    }

    std::vector<std::shared_ptr<RooAbsData>> channel_toys;
    std::map<std::string,RooDataSet*> cToys;

    RooArgSet _obs; _obs.add(*obs());
    if( !ws()->var("weightVar") ) { ws()->factory("weightVar[1]"); }
    _obs.add(*ws()->var("weightVar"));
    out.fData.reset( new RooDataSet(TString::Format("%s_%s",_pdf->GetName(),TUUID().AsString()),
            TString::Format("%s %s",_pdf->GetTitle(),(expected) ? "Expected" : "Toy"),
                                    _obs,"weightVar") );


    auto _cat = cat();
    if (_cat) {
        for(auto& [c,chan] : channels()) {
            auto toy = chan.generate(expected);
            if(toy.gobs() && _gobs) *_gobs = *toy.gobs();
            channel_toys.push_back(toy.fData); // need to keep alive
            _obs.setCatLabel(_cat->GetName(),c.c_str());
            for(int i = 0; i < toy->numEntries();i++) {
                _obs = *toy->get(i);
                out.fData->add(_obs, toy->weight());
            }
        }
    } else {
        // not a RooSimultaneous, just generate

        // must swap in the binning before generating, otherwise will end up using default binning
        std::unique_ptr<RooAbsBinning> binning;
        if (auto x = GetXaxis(); x) {
            if (auto xvar = dynamic_cast<RooRealVar *>(x->GetParent()); xvar) {
                binning.reset(xvar->getBinning().clone(xvar->getBinning().GetName()));
                xvar->setBinning(xvar->getBinning(x->GetName()));
            }
        }
        //TODO: handle case of no observables ... should make a RooDataSet with no obs and one poisson weighted event
        out.fData.reset( mainPdf()->generate(_obs, RooFit::Extended(), RooFit::ExpectedData(expected)) );
        out.fData->SetName(TString::Format("%s_%s",GetName(),TUUID().AsString()) );

        if(binning) {
            dynamic_cast<RooRealVar*>(GetXaxis()->GetParent())->setBinning(*binning);
            // range of variable in dataset may be less than in the workspace
            // if e.g. generate for a specific channel. So need to expand ranges to match
            for(auto o : *out.fData->get()) {
                auto x = dynamic_cast<RooRealVar*>(o);
                if (!x) continue;
                auto r = x->getRange();
                if (r.first > binning->lowBound()) x->setMin(binning->lowBound());
                if (r.second < binning->highBound()) x->setMax(binning->highBound());
            }
        }



        if (_gobs) {
            auto globs = _pdf->getObservables(*_gobs);
            if (!globs->empty()) {
                if(!expected) {
                    *_gobs = *std::unique_ptr<RooDataSet>(_pdf->generate(*globs, 1))->get();
                } else {

                    // loop over pdfs in top-level prod-pdf,
                    auto pp = dynamic_cast<RooProdPdf*>(_pdf);
                    if (pp) {
                        for(auto pdf : pp->pdfList()) {
                            auto gob = std::unique_ptr<RooArgSet>( pdf->getObservables(*globs) );
                            if (gob->empty()) continue;
                            if (gob->size()>1) {
                                Warning("generate","%s contains multiple global obs: %s",pdf->GetName(),gob->contentsString().c_str());
                                continue;
                            }
                            RooRealVar &rrv = dynamic_cast<RooRealVar &>(*gob->first());
                            std::unique_ptr<RooArgSet> cpars(pdf->getParameters(*globs));

                            bool foundServer = false;
                            // note : this will work only for this type of constraints
                            // expressed as RooPoisson, RooGaussian, RooLognormal, RooGamma
                            TClass * cClass = pdf->IsA();
                            if ( cClass != RooGaussian::Class() && cClass != RooPoisson::Class() &&
                                 cClass != RooGamma::Class() && cClass != RooLognormal::Class() &&
                                 cClass != RooBifurGauss::Class()  ) {
                                TString className =  (cClass) ?  cClass->GetName() : "undefined";
                                oocoutW((TObject*)0,Generation) << "AsymptoticCalculator::MakeAsimovData:constraint term "
                                                                << pdf->GetName() << " of type " << className
                                                                << " is a non-supported type - result might be not correct " << std::endl;
                            }

                            // in case of a Poisson constraint make sure the rounding is not set
                            if (cClass == RooPoisson::Class() ) {
                                RooPoisson * pois = dynamic_cast<RooPoisson*>(pdf);
                                assert(pois);
                                pois->setNoRounding(true);
                            }

                            // look at server of the constraint term and check if the global observable is part of the server
                            RooAbsArg * arg = pdf->findServer(rrv);
                            if (!arg) {
                                // special case is for the Gamma where one might define the global observable n and you have a Gamma(b, n+1, ...._
                                // in this case n+1 is the server and we don;t have a direct dependency, but we want to set n to the b value
                                // so in case of the Gamma ignore this test
                                if ( cClass != RooGamma::Class() ) {
                                    oocoutE((TObject*)0,Generation) << "AsymptoticCalculator::MakeAsimovData:constraint term "
                                                                    << pdf->GetName() << " has no direct dependence on global observable- cannot generate it " << std::endl;
                                    continue;
                                }
                            }

                            // loop on the server of the constraint term
                            // need to treat the Gamma as a special case
                            // the mode of the Gamma is (k-1)*theta where theta is the inverse of the rate parameter.
                            // we assume that the global observable is defined as ngobs = k-1 and the theta parameter has the name theta otherwise we use other procedure which might be wrong
                            RooAbsReal * thetaGamma = 0;
                            if ( cClass == RooGamma::Class() ) {
                                RooFIter itc(pdf->serverMIterator() );
                                for (RooAbsArg *a2 = itc.next(); a2 != 0; a2 = itc.next()) {
                                    if (TString(a2->GetName()).Contains("theta") ) {
                                        thetaGamma = dynamic_cast<RooAbsReal*>(a2);
                                        break;
                                    }
                                }
                                if (thetaGamma == 0) {
                                    oocoutI((TObject*)0,Generation) << "AsymptoticCalculator::MakeAsimovData:constraint term "
                                                                    << pdf->GetName() << " is a Gamma distribution and no server named theta is found. Assume that the Gamma scale is  1 " << std::endl;
                                }

                            }
                            RooFIter iter2(pdf->serverMIterator() );
                            for (RooAbsArg *a2 = iter2.next(); a2 != 0; a2 = iter2.next()) {
                                RooAbsReal * rrv2 = dynamic_cast<RooAbsReal *>(a2);
                                if (rrv2 && !rrv2->dependsOn(*gob) && !rrv2->isConstant() ) {


                                    // found server not depending on the gob
                                    if (foundServer) {
                                        oocoutE((TObject*)0,Generation) << "AsymptoticCalculator::MakeAsimovData:constraint term "
                                                                        << pdf->GetName() << " constraint term has more server depending on nuisance- cannot generate it " <<
                                                                        std::endl;
                                        foundServer = false;
                                        break;
                                    }
                                    if (thetaGamma && thetaGamma->getVal() > 0)
                                        rrv.setVal( rrv2->getVal() / thetaGamma->getVal() );
                                    else
                                        rrv.setVal( rrv2->getVal() );
                                    foundServer = true;

                                }
                            }

                            if (!foundServer) {
                                oocoutE((TObject*)0,Generation) << "AsymptoticCalculator::MakeAsimovData - can't find nuisance for constraint term - global observables will not be set to Asimov value " << pdf->GetName() << std::endl;
                                std::cerr << "Parameters: " << std::endl;
                                cpars->Print("V");
                                std::cerr << "Observables: " << std::endl;
                                gob->Print("V");
                            }

                        }
                    } else {
                        Error("generate","Cannot generate global observables");
                    }
                    *_gobs = *globs;
                }
            }
            delete globs;
        }
    }

    if(_gobs) {
        _gobs->setName(Form("%s_gobs",out.fData->GetName()));
        out.fGobs.reset(_gobs);
    }
    out.SetName(out.fData->GetName());
    return out;
}

RooAbsArg* Component::get() const {
    if (!fComp) {
        if (auto a = ws()->arg(GetName()); a) {
            fComp = std::shared_ptr<RooAbsArg>(a, [](RooAbsArg *) {}); // wont delete because owned by workspace
        }
    }
    return fComp.get();
}

bool Component::SetBin(int bin) const {
    if (bin<1) {
        if (bin==0 && !GetXaxis()) return true;
        return false;
    }
    if (!GetXaxis()) return false;
    if (bin > GetXaxis()->GetNbins()) return false;
    if (auto var = dynamic_cast<RooAbsRealLValue*>( GetXaxis()->GetParent() ); var) {
        var->setBin(bin-1, GetXaxis()->GetName() );
    } else if(auto var = dynamic_cast<RooAbsCategoryLValue*>( GetXaxis()->GetParent() ); var) {
        var->setIndex(bin-1);
    }
    return true;
}

TAxis* Component::GetXaxis() const {
    if (fXaxis) { return fXaxis.get(); }
    auto o = dynamic_cast<RooAbsReal*>(get());
    if (!o) return (fParent) ? fParent->GetXaxis() : nullptr;
    RooAbsLValue* x = cat(); // x-axis of RooSimultaneous is the category
    if (!x) {
        auto xvar = o->getStringAttribute("xvar");
        if (!xvar) {
            if (auto p = dynamic_cast<ParamHistFunc*>(o); p) {
                x = dynamic_cast<RooAbsLValue*>(p->_dataVars.at(0));
            } else if(auto p = dynamic_cast<RooHistFunc*>(o);p ) {
                x = dynamic_cast<RooAbsLValue*>(p->dataHist().get()->first());
            } else if (!o->getAttribute("blockLoop") && ((fData && fData->get()) || !samples().empty())) {
                 // will get obs listed from fData
                if(auto _obs = obs(); !_obs->empty()) {
                    x = dynamic_cast<RooAbsLValue *>(_obs->first());
                }
            } else if(auto _obs = obs(); _obs->size()==1){
                x = dynamic_cast<RooAbsLValue *>(_obs->first());
            } else {
                return nullptr;
            }
            // TODO: Use obs list of parent model to find first observable
            // could go to the parent but will instead use fData if available

        } else {
            x = dynamic_cast<RooAbsLValue *>(
                    std::unique_ptr<RooArgSet>(o->getParameters(RooArgSet()))->find(xvar));
        }
    }
    if (!x) {
        return nullptr;
    }
    auto binningName = o->getStringAttribute("binning");

    if(auto r = dynamic_cast<RooAbsRealLValue*>(x);r) {
        if (r->getBinning(binningName).isUniform()) {
            fXaxis = std::make_shared<Axis>(x->numBins(binningName), r->getMin(binningName), r->getMax(binningName));
        } else {
            fXaxis = std::make_shared<Axis>(x->numBins(binningName), r->getBinning(binningName).array());
        }
    } else {
        std::vector<double> bins = {};
        for(int i = 0; i <= x->numBins(binningName);i++) bins.push_back(i);
        fXaxis = std::make_shared<Axis>(x->numBins(binningName),&bins[0]);
        // TODO have to load current state of bin labels if was a category (sadly not a virtual method)
        for(int i=0; i<x->numBins(binningName);i++) {

        }
    }
    fXaxis->SetName(binningName ? binningName : "");
    fXaxis->SetParent(dynamic_cast<TObject*>(x));

    return fXaxis.get();
}

std::shared_ptr<RooArgSet> Component::globs(const FitResult& fr) const {

    auto _pdf = dynamic_cast<RooAbsPdf*>(get());
    if (!_pdf) return nullptr;

    auto _obs = obs(fr);
    RooArgSet* gobs_and_np = _pdf->getParameters(*_obs);

    auto _gobs = std::make_shared<RooArgSet>("globalObservables");
    _gobs->add(*gobs_and_np); //will remove the np in a moment

    //now pass this to the getAllConstraints method ... it will modify it to contain only the np!
    RooArgSet* s = _pdf->getAllConstraints(*_obs,*gobs_and_np /*,false ... could use this to get rid of warning - keep disconnected constraints when no obs pdf term*/);
    if(s->empty()) {
        // fallback to using the flag
        _gobs->remove(*std::unique_ptr<RooAbsCollection>(_gobs->selectByAttrib("isGlobalObs",false)));
    } else {
        _gobs->remove(*gobs_and_np);
    }
    delete s; //don't ever need this

    delete gobs_and_np;
    return _gobs;

}

ComponentCollection Component::pars() const {
    ComponentCollection out;
    if (!get()) return out;
    RooArgSet _allobs;
    if(auto ob = obs(); ob) _allobs.add( *ob );
    if(auto glob = globs(); glob) _allobs.add( *glob );
    auto _pars = std::unique_ptr<RooArgSet>( get()->getParameters(_allobs) );
    for(auto& p : *_pars) {
        if (p->getAttribute("Constant")) continue; // these are "args"
        out.push_back(Component(*p,*this));
    }
    return out;
}

ComponentCollection Component::args() const {
    ComponentCollection out;
    if (!get()) return out;
    RooArgSet _allobs;
    _allobs.add( *obs() );
    _allobs.add( *globs() );
    auto _pars = std::unique_ptr<RooArgSet>( get()->getParameters(_allobs) );
    for(auto& p : *_pars) {
        if (!p->getAttribute("Constant")) continue; // these are "args"
        out.push_back(Component(*p,*this));
    }
    // also need to get all constPars
    RooArgSet allLeafs;
    get()->leafNodeServerList(&allLeafs);
    for(auto c : allLeafs) {
        if (dynamic_cast<RooConstVar*>(c)) out.push_back(Component(*c,*this));
    }
    return out;
}

Component Component::constraint() const {
    auto o = getConstraint();
    if (!o) return Component("",*this);
    return Component(*o,*this);
}

std::map<std::string,Component> Component::constraints() const {
    std::map<std::string,Component> out;
    auto o = dynamic_cast<RooAbsPdf*>(get());
    if(!o) return out;
    auto _mainPdf = mainPdf();
    if (!_mainPdf || _mainPdf == o) return out;
    if(auto p = dynamic_cast<RooProdPdf*>(o); p) {
        for(auto& pdf : p->pdfList()) {
            if(pdf == _mainPdf) continue;
            // determine which parameter of the pdf this pdf constrains, by getting all parameters and find dependent one
            auto allPars = std::unique_ptr<RooArgSet>(pdf->getParameters(RooArgSet()));
            auto depPars = std::unique_ptr<RooArgSet>(_mainPdf->getDependents(*allPars));
            if (depPars->empty()) continue; // unusual, no parameters are constrained?
            if (depPars->size()==1) {
                out[ depPars->first()->GetName() ] = Component(*pdf, *this);
            } else {
                Warning("constraints","%s constrains multiple parameters? : %s",pdf->GetName(),depPars->contentsString().c_str());
            }
        }
    }
    return out;
}

std::shared_ptr<RooArgSet> Component::obs(const FitResult& fr) const {
    auto o = get();
    if (!o) return nullptr;


    if (auto p = dynamic_cast<RooProduct*>(o); p) {
        auto out = std::make_shared<RooArgSet>();
        for(auto c : p->_compRSet) {
          if (dynamic_cast<RooHistFunc*>(c) || dynamic_cast<PiecewiseInterpolation*>(c) || dynamic_cast<RooProduct*>(c)) {
              out->add( *Component(*c,*this).obs(fr), true );
          }
        }
        return out;
    } /*else if (dynamic_cast<RooRealVar*>(o)) { // parameters are not observables
        return std::make_shared<RooArgSet>(*o);
    }*/ else if(auto h = dynamic_cast<RooHistFunc*>(o);h) {
        return std::make_shared<RooArgSet>( h->_depList );
    } else if(auto h = dynamic_cast<PiecewiseInterpolation*>(o); h) {
        return Component(*h->_nominal.absArg(),*this).obs(fr);
    } else if(auto h = dynamic_cast<RooProdPdf*>(o); h && !mainPdf()) {
        // product of pdfs with no main pdf, so take the observables of all the pdfs
        auto out = std::make_shared<RooArgSet>();
        for(auto p : h->pdfList()) {
            out->add( *Component(*p,*this).obs(fr), true );
        }
        return out;
    } else if(auto p = dynamic_cast<RooGaussian*>(o);p) {
        return Component( *p->x.absArg(),*this ).obs(fr);
    } else if(auto p = dynamic_cast<RooPoisson*>(o);p) {
        return Component( *p->x.absArg(),*this ).obs(fr);
    }

    auto out = std::make_shared<RooArgSet>();out->setName(Form("obs_%s",GetName()));
    if(auto c = cat(); c) out->add(*c);
    for(auto& s : samples()) out->add( *s.obs(fr), true );
    for(auto& s : channels()) out->add( *s.second.obs(fr), true );
    /*don't include globs in this set because not sure if having them in the calls to getVal(normSet) will screw things
     * for(auto& s : constraints()) {
        if(auto p = dynamic_cast<RooGaussian*>(s.second.get());p) {
            out->add( *p->x.absArg() );
        } else if(auto p = dynamic_cast<RooPoisson*>(s.second.get());p) {
            out->add( *p->x.absArg() );
        }
    }*/


    return out;

    if (fData) {
        RooArgSet s;
        s.add(*(fData->get())->get());
        //if (fData->gobs()) s.add(*fData->gobs());
        return std::shared_ptr<RooArgSet>( get()->getObservables(s) );
    }
    // if has an xvar then return that as the observable
    auto allPars = std::make_shared<RooArgSet>();
    get()->setAttribute("blockLoop"); // to prevent infinite loop
    if (GetXaxis()) {
        allPars->add(*dynamic_cast<RooAbsArg*>(GetXaxis()->GetParent()));
    }
    get()->setAttribute("blockLoop",false);
    for(auto& c : channels()) {
        auto s = c.second.obs(fr);
        if (s) allPars->add( *s );
    }
    if (!allPars->empty()) {
        return allPars;
    }
    allPars->add( *std::unique_ptr<RooArgSet>(get()->getParameters(RooArgSet())).get() );
    if(fr->_constPars) allPars->remove(fr->constPars(),true,true);
    if(fr->_finalPars) allPars->remove(fr->floatParsFinal(),true,true);
    return allPars;
}

RooArgList Component::scaleFactors(int bin) const {
    RooArgList l;
    auto ax = GetXaxis(); if (!ax) return l;
    if (ax->GetNbins() <= bin) {
        Error("shapeFactors","Invalid bin number: %d", bin);
        return l;
    }
    auto p = dynamic_cast<RooProduct*>(get());
    if (!p) return l;
    // find all ParamHistFuncs in product terms
    ParamHistFunc* f = 0;
    for(auto c : p->_compRSet) {
        if (f = dynamic_cast<ParamHistFunc*>(c); f) {
            if (auto pr = dynamic_cast<RooProduct*>(f->_paramSet.at(bin)); pr) {
                l.add(pr->components());
            } else if(strcmp(f->_paramSet.at(bin)->GetName(),"1")) {
                l.add(*f->_paramSet.at(bin));
            }
        }
    }

    return l;

}

bool Component::Scale(int bin, RooAbsReal& func) {
    auto ax = GetXaxis(); if (!ax) return false;
    auto x = dynamic_cast<RooRealVar*>(ax->GetParent()); if (!x) return false;
    auto p = dynamic_cast<RooProduct*>(get());
    if (!p) return false;
    // find the generic shapeFactors ParamHistFunc in product terms
    ParamHistFunc* f = 0;
    for(auto c : p->_compRSet) {
        if (f = dynamic_cast<ParamHistFunc*>(c); f && strcmp(f->GetTitle(),"shapeFactors")==0) { break; }
        else { f = nullptr; }
    }
    if (!f) {
        // create a new ParamHistFunc, all bins will be "1" except for our desired bin
        RooArgList list;
        for(int i = 0; i < ax->GetNbins(); i++) {
            list.add( (i==bin-1) ? func : *std::dynamic_pointer_cast<RooRealVar>(acquire(std::make_shared<RooRealVar>("1","1",1))));
        }
        // paramhistfunc requires the binnings to be loaded as default at construction time
        // so load binning temporarily
        auto tmp = dynamic_cast<RooAbsBinning*>(dynamic_cast<RooAbsLValue*>(ax->GetParent())->getBinningPtr(0)->Clone());
        x->setBinning( x->getBinning(ax->GetName()) );
        TString fName = Form("shapeFactors_%s",GetName());
        auto phf = acquire( std::make_shared<ParamHistFunc>(fName,"shapeFactors",*x,list) );
        x->setBinning( *tmp ); // restore binning
        delete tmp;
        p->_compRSet.add(*std::dynamic_pointer_cast<RooAbsArg>(phf) );
        dynamic_cast<ParamHistFunc*>(phf.get())->_paramSet.setName("paramSet"); // so can see when print
    } else {
        // replace ith element in list with new func, or inject into RooProduct
        RooArgList all;
        for(int i = 0; i < f->_paramSet.getSize();i++) {
            if (i!=bin-1) all.add(*f->_paramSet.at(i));
            else {
                if (auto pr = dynamic_cast<RooProduct*>(f->_paramSet.at(i)); pr) {
                    pr->_compRSet.add(func);
                    pr->_cacheMgr.reset();
                } else if(strcmp(f->_paramSet.at(i)->GetName(),"1")==0) {
                    // can just replace because it's the unit value
                    all.add(func);
                } else {
                    auto p = acquire(std::make_shared<RooProduct>(Form("%s_bin%d", f->GetName(), bin+1), "", RooArgList(*f->_paramSet.at(i), func)));
                    all.add(*std::dynamic_pointer_cast<RooAbsArg>(p));
                }
            }
        }
        f->_paramSet.removeAll();
        f->_paramSet.add(all);
    }
    get()->setValueDirty();
    return true;
}

bool Component::Unscale(const std::string& factorName) {
    auto f = factor(factorName);
    if (auto a = dynamic_cast<RooAbsReal*>(f.get())) return Unscale(0,*a);
    return true;
}

bool Component::Unscale(int bin, RooAbsReal& func) {
    auto ax = GetXaxis(); if (bin!=0 && !ax) return false;
    auto p = dynamic_cast<RooProduct*>(get());
    if(bin==0 && p) {
        p->_compRSet.remove(func);
    } else {
        // find ParamHistFunc in product terms
        auto removeFromPHF = [&](RooAbsArg* c) {
            if (auto f = dynamic_cast<ParamHistFunc *>(c); f) {
                if (auto pr = dynamic_cast<RooProduct *>(f->_paramSet.at(bin - 1)); pr) {
                    if (pr->_compRSet.size() == 1) { pr->_compRSet.replace(func, *acquire<RooRealVar>("1", "1", 1));
                    } else pr->_compRSet.remove(func);
                    pr->_cacheMgr.reset();
                } else if (f->_paramSet.index(func) == bin - 1) {
                    f->_paramSet.replace(func, *acquire<RooRealVar>("1", "1", 1));
                }
            }
        };

        if (!p) {
            if (!dynamic_cast<ParamHistFunc*>(get())) return false;
            removeFromPHF(get());
        } else {
            for (auto c : p->_compRSet) {
                removeFromPHF(c);
            }
        }
    }
    if (auto o = get(); bin==0 && o && o->getStringAttribute("factors")) {
        o->setStringAttribute("factors", TString(o->getStringAttribute("factors")).ReplaceAll(TString::Format("%s;",func.GetName()),""));
    }
    return true;
}

RooArgList Component::scaleFactors() const {
    RooArgList l;
    auto p = dynamic_cast<RooProduct*>(get());
    if (!p) return l;
    l.add( p->_compRSet );
    return l;
}

RooAbsReal* Component::coef() const {
    if (!fParent) return nullptr;
    if (auto o = dynamic_cast<RooRealSumPdf*>(fParent->mainPdf()); o) {
        return dynamic_cast<RooAbsReal*>( o->coefList().at( o->funcList().index(GetName()) ) );
    }
    return nullptr;
}

double Component::GetBinContent(int i, const FitResult& fr) const {
    if (i<0) return std::numeric_limits<double>::quiet_NaN();
    auto o = dynamic_cast<RooAbsReal*>(get());
    if (!o) return std::numeric_limits<double>::quiet_NaN();
    if (!SetBin(i)) return std::numeric_limits<double>::quiet_NaN(); // means was out of range

    std::unique_ptr<RooArgSet> snap = nullptr;
    std::unique_ptr<RooArgSet> deps = nullptr;
    if (fr->_finalPars) {
        deps.reset(o->getDependents(*fr->_finalPars) );
        snap.reset(deps->snapshot());
        *deps = *fr->_finalPars;
    }

    double out = 1;
    // find all observables from the fr
    auto myObs = obs(fr);
    if(auto c = coef(); c) out = c->getVal(myObs.get());
    auto v = (GetXaxis()) ? dynamic_cast<RooAbsRealLValue*>(GetXaxis()->GetParent()) : nullptr;

    if (!o->getAttribute("isFactor") && GetXaxis()) {
        out *= GetXaxis()->GetBinWidth(i);
    }
    if( auto p = dynamic_cast<RooAbsPdf*>(o) ; p) {
        //auto v = dynamic_cast<RooAbsArg*>(GetXvar());RooArgSet s; if(v) s.add(*v); -- tried using this for normSet instead
        // only for RooSimultaneous do we then divide out the current pdf value
        // this is so that bin content is integral in that channel
        if (auto s = dynamic_cast<RooSimultaneous*>(p); s) {
            // FIXME: what if value is 0 in the channel!!
            RooArgSet ss;
            if (myObs) ss.add(*myObs);
            ss.remove(s->indexCat());
            out *= s->expectedEvents(ss);
        } else {
            if(v && (!p->isBinnedDistribution(*v) || p->getAttribute("intBC"))) {
                std::cout << "Performing experimental running integral ... currently not working properly!" << std::endl;
                // experimental feature:
                // may prefer to integrate across the bin here rather than assume it's flat
                // if this were a RooBinSamplingPdf then it should be doing it magically for us!!
                // In future ROOT, fitting a RooDataHist to a continuous pdf will trigger creation of the RooBinSamplingPdf
                // so this integration step will be appropriate (see RooAbsOptTestStatistic)
                // FIXME: This doesn't seem to integrate properly if use IntRI ...
                double lo = GetXaxis()->GetBinLowEdge(i)-1e-12;
                double hi = GetXaxis()->GetBinUpEdge(i);
                // - NOT WORKING auto ri = std::unique_ptr<RooAbsReal>(dynamic_cast<RooAbsReal*>(robj())->createIntRI(*v,*fWS.obs()));
                // only a scanRI seems to be behave well
                auto ri = std::unique_ptr<RooAbsReal>(dynamic_cast<RooAbsReal*>(o)->createScanRI(*v, *myObs, 1000, 0));
                v->setVal(hi);
                out = ri->getVal(); // effectively includes bin width so override current out value
                if (lo > v->getMin()) {v->setVal(lo);out -= ri->getVal();}
            } else {
                out *= o->getVal(myObs.get());
            }
            /*if (!p->selfNormalized())*/ out *= p->expectedEvents(myObs.get());
        }
        p->_normSet = nullptr; // clearing so don't remember deleted myObs (causes crash issues in Print methods)

    } else {
        out *= o->getVal(); // this is a density
    }

    if (snap) {
        *deps = *snap;
    }

    return out;
}

double Component::Integral(const FitResult& fr) const {
    //auto x = dynamic_cast<RooAbsArg*>(GetXvar());
    //if (!x) {return GetBinContent(0);} // just return value
    /*if( auto p = dynamic_cast<RooAbsPdf*>(robj()); p) {
     * seems to lead to bad caches
        return p->getNorm(*x); // do this to utilize caching of integration by pdfs
    }*/
    double out = 1;
    auto _obs = obs(fr);
    if(auto c = coef(); c) out = c->getVal(_obs.get()); // assumes independent of observables!
    if (auto p = dynamic_cast<RooAbsPdf*>(get()); p) {
        // prefer to use expectedEvents for integrals of RooAbsPdf e.g. for RooProdPdf wont include constraint terms
        return p->expectedEvents(*_obs) * out;
    }
    if( auto p = dynamic_cast<RooAbsReal*>(get()); p) {
        // only integrate over observables we actually depend on
        auto f = std::unique_ptr<RooAbsReal>(p->createIntegral(*std::unique_ptr<RooArgSet>( p->getObservables(*_obs)).get())); // did use x here before using obs
        return f->getVal()*out;
    }
    return std::numeric_limits<double>::quiet_NaN();
}

class PdfWrapper : public RooAbsPdf {
public:
    PdfWrapper(RooAbsPdf& f, RooAbsReal* coef) : RooAbsPdf(Form("exp_%s",f.GetName())), fFunc("func","func",this,f), fCoef("coef","coef",this) {
        if (coef) fCoef.setArg(*coef);
    }
    virtual ~PdfWrapper() { };
    PdfWrapper(const PdfWrapper& other, const char* name=0) : RooAbsPdf(other,name), fFunc("func",this,other.fFunc) { }
    virtual TObject* clone(const char* newname) const override { return new PdfWrapper(*this,newname); }
    Bool_t isBinnedDistribution(const RooArgSet& obs) const override { return fFunc->isBinnedDistribution(obs); }
    std::list<Double_t>* binBoundaries(RooAbsRealLValue& obs, Double_t xlo, Double_t xhi) const override {
        return fFunc->binBoundaries(obs,xlo,xhi);
    }

    double evaluate() const override {
        return fFunc*(dynamic_cast<RooAbsPdf*>(fFunc.absArg())->expectedEvents(_normSet))*(fCoef.absArg()?fCoef : 1.);
    }
private:
    RooRealProxy fFunc;
    RooRealProxy fCoef;
};


double Component::GetBinError(int bin, const FitResult& fr) const {
    if (!SetBin(bin)) { return std::numeric_limits<double>::quiet_NaN(); }

    auto o = dynamic_cast<RooAbsReal*>(get());
    if (!o) return std::numeric_limits<double>::quiet_NaN();

    if (!fr->_finalPars) {
        fr->setFinalParList(RooArgList());
    }


    // need to add any floating parameters not included somewhere already in the fit result ...
    // TODO This is modifying the underlying fit result, we shouldn't do that!
    RooArgList l;
    for(auto& [k,p] : pars()) {
        if (fr->floatParsFinal().find(p.GetName())) continue;
        if (fr->_constPars && fr->_constPars->find(p.GetName())) continue;
        l.add(*p.get());
    }

    if (!l.empty()) {
        RooArgList l2; l2.addClone(fr->floatParsFinal());
        l2.addClone(l);
        fr->setFinalParList(l2);
    }


    auto normSet = obs();

    if (!fr->_VM || fr->_VM->GetNcols() < fr->floatParsFinal().size()) {
        TMatrixDSym cov(fr->floatParsFinal().getSize());
        auto prevCov = fr->_VM;
        if (prevCov) {
            for(int i =0;i<prevCov->GetNcols();i++) {
                for(int j=0;j<prevCov->GetNrows();j++) {
                    cov(i,j) = (*prevCov)(i,j);
                }
            }
        }
        int i = 0;
        for(auto& p : fr->floatParsFinal()) {
            if (!prevCov || i>=prevCov->GetNcols()) {
                cov(i, i) = pow(dynamic_cast<RooRealVar *>(p)->getError(), 2);
            }
            i++;
        }
        int covQualBackup = fr->_covQual;
        fr->setCovarianceMatrix(cov);
        fr->_covQual = covQualBackup;

    }



    double res;
    if (auto p = dynamic_cast<RooAbsPdf*>(o); p) {
        res = PdfWrapper(*p, coef()).getPropagatedError(*fr.get(), (normSet ? *normSet : RooArgSet()));
        p->_normSet = nullptr;
    } else {
        res = o->getPropagatedError(*fr.get(),(normSet ? *normSet : RooArgSet()));
        // TODO: What if coef has error? - probably need a FuncWrapper class
        if (auto c = coef(); c) { res *= c->getVal((normSet ? *normSet : RooArgSet())); }

    }


    if(auto ax = GetXaxis(); ax && !kIsFactor) res *= ax->GetBinWidth(bin);

    return res;

}


void Component::DividePad(const std::string& channels, int ncols) {
    std::vector<TString> chans;
    TStringToken pattern(channels, ";");
    while (pattern.NextToken()) {
        chans.emplace_back(pattern);
    }
    if (ncols == 0) ncols = std::ceil(sqrt(chans.size()));
    if (!gPad) TCanvas::MakeDefCanvas();
    gPad->Divide(ncols,chans.size()/ncols + (chans.size()%ncols>0),1e-12,1e-12);
    for(int i=0;i<chans.size();i++) {
        gPad->GetPad(i+1)->SetName(chans[i]);
    }
}

TH1* Component::BuildHistogram(const FitResult& fr) {
    TH1D* out = nullptr;
    auto ax = GetXaxis();
    if (!ax) {
        out = new TH1D(GetName(),GetTitle(),1,0,1);
    } else {
        if (dynamic_cast<Axis*>(ax)->binning()) {
            out = new TH1D(GetName(), GetTitle(), ax->GetNbins(), dynamic_cast<Axis *>(ax)->binning()->array());
        } else {
            // category binnings are uniform
            out = new TH1D(GetName(), GetTitle(), ax->GetNbins(), 0, ax->GetNbins() );
        }
        out->GetXaxis()->SetName(ax->GetParent()->GetName());
        out->GetXaxis()->SetTitle( ax->GetTitle() );
    }
    out->Sumw2();

    auto o = get();

    out->SetDirectory(0);
    bool hasError = false;
    for(int i = 1;i <= out->GetNbinsX();i++) {
        out->SetBinContent(i, GetBinContent((ax) ? i : (i-1),fr));
        auto err = GetBinError((ax) ? i : (i-1),fr);
        if (err) hasError=true;
        out->SetBinError(i,err);
    }
    if (!hasError) { out->SetBinError(1,1e-15); } // here just to help ROOT not 'fill'


    if (o) {
        if(auto s = o->getStringAttribute("FillColor"); s) {
            out->SetFillColor(TString(s).Atoi());
        }
    }
    out->GetYaxis()->SetTitle("Events");
    out->GetYaxis()->SetMaxDigits(3);
    return out;
}

std::map<std::string,Component> Component::channels() const {
    std::map<std::string,Component> out;
    auto s = dynamic_cast<RooSimultaneous*>(get());
    if (!s) return out;
    for(auto c : s->indexCat()) {
        if(auto p = s->getPdf(c.first.c_str());p) {
            (out[c.first] = Component(p->GetName(),*this)).kIsChannel = true;
        }
    }
    return out;
}

Component Component::channel(const std::string& channelName) const {

    auto chans = channels();
    if (auto itr = chans.find(channelName); itr != chans.end()) {
        return itr->second;
    }
    Component out(Form("%s_%s",GetName(),channelName.c_str()),*this);
    out.SetTitle(channelName.c_str());
    out.kIsChannel=true;
    return out;
}

// this can return an empty factor if there are no bin-wise factors
ComponentCollection Component::factors(int bin) const {
    if (bin==0) return factors(); // overall factors
    ComponentCollection out;
    // look for ParamHistFuncs and pick out the resulting factors
    auto o = get();
    if (auto f = dynamic_cast<ParamHistFunc*>(o); f) {
        if(f->_paramSet.size() >= bin) {
            for(auto& [k,c] : Component(*f->_paramSet.at(bin-1),*this).factors(0)) {
                if (strcmp(c.GetName(),"1")) out.push_back(c);
            }
        }
    } else if(auto f = dynamic_cast<RooProduct*>(o); f) {
        for(auto p : f->_compRSet) {
            for(auto& [k,c] : Component(*p,*this).factors(bin)) {
                if (strcmp(c.GetName(),"1")) out.push_back(c);
            }
        }
    }
    return out;

}

// this will always return itself if the component cannot be factorized
ComponentCollection Component::factors() const {
    ComponentCollection out;
    auto p = dynamic_cast<RooProduct*>(get());
    if (!p) {
        out.push_back(*this);
        return out;
    }
    ComponentCollection tmpout; // used to put nominal or piecewise part first
    RooAbsArg* bestFactor = nullptr;
    auto addComp = [&](RooAbsArg* a) {
        if (!strcmp(a->GetName(),"1")) return; // don't add the "1" factor
        if (dynamic_cast<PiecewiseInterpolation*>(a) || TString::Format("%s_nominal",GetName())==a->GetName()) {
            if (!bestFactor ||
                (strcmp(bestFactor->GetTitle(),GetTitle()) && !strcmp(a->GetTitle(),GetTitle())) ||
                TString::Format("%s_interp",GetName())==a->GetName()
                    ) {
                if (bestFactor) {
                    tmpout.push_back(Component(*bestFactor,*this));
                    tmpout.back().kIsFactor=true;
                    tmpout.back().kIsVarFactor= dynamic_cast<ParamHistFunc*>(bestFactor) || dynamic_cast<RooRealVar*>(bestFactor);
                }
                bestFactor = a;
                return;
            }
        }
        tmpout.push_back(Component(*a,*this));
        tmpout.back().kIsFactor=true;
        tmpout.back().kIsVarFactor= dynamic_cast<ParamHistFunc*>(a) || dynamic_cast<RooRealVar*>(a);
    };
    for(auto c : p->_compRSet) {
        if (auto pp = dynamic_cast<RooProduct*>(c); pp) {
            for(auto& [k,f] : Component(*c,*this).factors()) {
                addComp(f.get());
            }
        } else {
            addComp(c);
        }
    }
    if (!bestFactor) return tmpout;
    out.push_back(Component(*bestFactor,*this));
    for(auto& f : tmpout) { out.push_back(f); }
    return out;
}


Component Component::factor(const std::string& factorName) const {

    Component o("");
    for(auto& [k,s] : factors()) {
        if (factorName == s.GetName()) {
            return s;
        }
        if (factorName == s.GetTitle()) {
            o = s;
        }
    }
    if (strlen(o.GetName())) return o;

    // TODO should assert that this object doesn't exist yet
    Component out(Form("%s_%s", GetName(), factorName.c_str()), *this);
    out.SetTitle(factorName.c_str());
    out.kIsFactor = true;
    return out;
}

Component Component::varFactor(const std::string& factorName) const {
    auto out = factor(factorName);
    if(out.get() && !dynamic_cast<ParamHistFunc*>(out.get()) && !dynamic_cast<RooRealVar*>(out.get())) {
        Error("varFactor", "%s exists but is not a varFactor", out.GetName());
        return Component("");
    }
    out.kIsVarFactor=true; // so when creating will create a ParamHistFunc
    return out;
    return factor(factorName);
}

Component Component::parFactor(const std::string& factorName) const {
    return factor(factorName);
}

Component Component::sample(const std::string& sampleTitle) const {
    for(auto s : samples()) {
        if (sampleTitle==s.GetTitle()) { return s; }
    }
    // TODO should assert that this object doesn't exist yet
    Component out(Form("%s_%s",GetName(),sampleTitle.c_str()),*this);
    out.SetTitle(sampleTitle.c_str());
    return out;
}

std::shared_ptr<TObject> Component::acquire(const std::shared_ptr<TObject>& arg) {
    if (fParent) return fParent->acquire(arg);
    ProfileClock c("acquire");
    // if has a workspace and our object is in the workspace then add this object to workspace
    if (ws() && ( ws()->arg(GetName()) || (arg && strcmp(arg->GetName(),GetName())==0))) {
        RooFit::MsgLevel msglevel = RooMsgService::instance().globalKillBelow();
        RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
        if (auto a = dynamic_cast<RooAbsArg*>(arg.get()); a) {
            auto out_arg = ws()->arg(arg->GetName());
            if (!out_arg){
                ProfileClock cc("acquire_import");
                if (ws()->import(*a, RooFit::RecycleConflictNodes())) {
                    RooMsgService::instance().setGlobalKillBelow(msglevel);
                    return nullptr;
                }
                sanitizeWS(); // clears the caches that might exist up to now, as well interfere with getParameters calls
                out_arg = ws()->arg(arg->GetName());
            }
            RooMsgService::instance().setGlobalKillBelow(msglevel);
            return std::shared_ptr<TObject>(out_arg, [](TObject*){});
        } else if(auto a = dynamic_cast<RooAbsData*>(arg.get()); a) {
            if (ws()->import(*a, RooFit::Embedded())) {
                RooMsgService::instance().setGlobalKillBelow(msglevel);
                return nullptr;
            }
            RooMsgService::instance().setGlobalKillBelow(msglevel);
            return std::shared_ptr<TObject>(ws()->embeddedData(arg->GetName()), [](TObject*){});
        }
        RooMsgService::instance().setGlobalKillBelow(msglevel);
        Warning("acquire","Not implemented acquisition of object %s",arg->GetName());
        return nullptr;
    }
    std::cout << GetName() << " taking over " << arg->GetName() << std::endl;
    fOwned.push_back(arg);
    return arg;
}

std::shared_ptr<TObject> Component::getObject(const std::string& name) const {
    if (fParent) return fParent->getObject(name);
    for(auto& o : fOwned) {
        if (o && name==o->GetName()) return o;
    }
    if (ws()) {
        if(auto arg = ws()->arg(name.c_str()); arg) {
            return std::shared_ptr<TObject>(arg,[](TObject*){});
        } else if(auto arg = ws()->data(name.c_str()); arg) {
            return std::shared_ptr<TObject>(arg,[](TObject*){});
        } else if(auto arg = ws()->genobj(name.c_str()); arg) {
            return std::shared_ptr<TObject>(arg,[](TObject*){});
        } else if(auto arg = ws()->embeddedData(name.c_str()); arg) {
            return std::shared_ptr<TObject>(arg,[](TObject*){});
        }
    }
    return nullptr;
}

std::shared_ptr<RooRealVar> Component::var(const std::string& name) const { return getObject<RooRealVar>(name); }

void Component::sterilize() {
    auto o = get();
    if (auto p = dynamic_cast<RooProdPdf*>(o); p) {
        p->_cacheMgr.reset();
    }
    if (auto p = dynamic_cast<RooAbsPdf*>(o); p) {
        p->_normSet = nullptr; // this is the key one for RooProdPdf::Print issues ... this lingers after calls to getVal
        p->_normMgr.reset();
    }
}

bool Component::SetContent(TH1* hist, const char* varPar, bool high) {
    bool made = false;
    if (!get()) {
        made = true;
        // has a parent ... must exist
        if (fParent && !fParent->get()) {
            std::unique_ptr<TH1> parentHist;
            if (kIsChannel) parentHist.reset( new TH1D("channelCat","channelCat;channelCat",1,0,1) );
            if(!fParent->SetContent(parentHist.get())) {
                Error("create","Could not get or create parent: %s",fParent->GetName());
                return false;
            }
        }

        // if no parent and null hist then create a RooSimultaneous
        if (!fParent) {
            if(hist && strcmp(hist->GetName(),"channelCat")==0) {
                auto cat = getObject<RooCategory>("channelCat");
                if (!cat) {
                    cat = acquire<RooCategory>("channelCat", "channels");
                }
                fComp = acquire<RooSimultaneous>(GetName(), GetTitle(), *cat);
                return true;
            }
            // if get here will end up creating either
            // a RooHistFunc (if hist and not varFactor)
            // a FlexibleInterpVar (if no hist and not varFactor)
            // a RooRealVar (if no hist but is varFactor)
            // ParamHistFunc (if hist and varFactor)
        }
        if (fParent) {
            // if parent is a simultaneous, create a RooProdPdf for
            if(auto p = dynamic_cast<RooSimultaneous*>(fParent->get()); p) {
                // add to parent with category label equal to name with parentage stripped
                std::string catLabel = TNamed::GetTitle();
                if (!fParent->cat()->hasLabel(catLabel)) {
                    fParent->cat()->defineType(catLabel.c_str());
                    // need to update any datasets that have this too
                    for (auto d : ws()->allData()) {
                        auto c = dynamic_cast<RooAbsCategoryLValue *>(d->get()->find(fParent->cat()->GetName()));
                        if (c) c->defineType(catLabel.c_str());
                    }
                }
                fComp = acquire<RooProdPdf>(GetName(), GetTitle(), RooArgList());
                p->addPdf(*dynamic_cast<RooAbsPdf *>(get()), catLabel.c_str());
                return true;
            }
        }

        // need to create RooHistFunc before setting content


        RooRealVar* x = (fParent && fParent->GetXaxis()) ? dynamic_cast<RooRealVar*>(fParent->GetXaxis()->GetParent()) : nullptr;
        std::string binningName;
        if (hist) {
            if (!x) {
                // get or create a new var
                TString vName = hist->GetXaxis()->GetName();
                if (vName.Contains(";")) vName = vName(vName.Contains(";") + 1, vName.Length());
                x = getObject<RooRealVar>(vName.Data()).get();
                if (!x) {
                    x = acquire<RooRealVar>(vName, hist->GetXaxis()->GetTitle(), hist->GetXaxis()->GetXmin(),
                                            hist->GetXaxis()->GetXmax()).get();
                }
                if (!x) {
                    Error("SetContent", "soemthing went wrong");
                    return false;
                }
                // ensure var range covers histogram range
                auto r = x->getRange();
                if (r.first > hist->GetXaxis()->GetXmin()) x->setMin(hist->GetXaxis()->GetXmin());
                if (r.second < hist->GetXaxis()->GetXmax()) x->setMax(hist->GetXaxis()->GetXmax());
            }
            binningName = hist->GetXaxis()->GetName();
            if (binningName.find(";") == std::string::npos) {
                if (fParent) {
                    binningName = fParent->GetXaxis() ? GetName()
                                                      : fParent->GetName(); // use sample name for binning name or channel name if that doesn't have a binning yet
                }
            } else {
                binningName = binningName.substr(binningName.find(";") + 1);
            }
            if (fParent && !fParent->GetXaxis()) {
                fParent->get()->setStringAttribute("xvar", x->GetName());
                fParent->get()->setStringAttribute("binning", binningName.c_str());
            }

            // add the binning to match this sample's name or the parent name if it's the first binning
            if (!x->hasBinning(binningName.c_str())) {
                if (hist->GetXaxis()->IsVariableBinSize()) {
                    x->setBinning(RooBinning(hist->GetNbinsX(), hist->GetXaxis()->GetXbins()->GetArray()),
                                  binningName.c_str());
                } else {
                    x->setBinning(RooUniformBinning(hist->GetXaxis()->GetXmin(), hist->GetXaxis()->GetXmax(),
                                                    hist->GetXaxis()->GetNbins()), binningName.c_str());
                }
                x->getBinning(binningName.c_str()).SetTitle(hist->GetXaxis()->GetTitle());
            }
            if (kIsVarFactor) {
                // create a ParamHistFunc with the given binning
                // will create parameters for each bin
                // unless the histogram is completely empty in which case we create a set of 1s
                RooArgList list;
                for(int i = 0; i < x->getBinning(binningName.c_str()).numBins(); i++) {
                    list.add( *(
                            (hist->GetEntries()==0) ? acquire<RooRealVar>("1", "1", 1) :
                            acquire<RooRealVar>(TString::Format("%s_bin%d",GetName(),i+1),"",hist->GetBinContent(i+1))));
                }
                // paramhistfunc requires the binnings to be loaded as default at construction time
                // so load binning temporarily
                auto tmp = dynamic_cast<RooAbsBinning*>(x->getBinningPtr(0)->Clone());
                x->setBinning( x->getBinning(binningName.c_str()) );
                fComp = acquire<ParamHistFunc>(GetName(),GetTitle(),*x,list );
                x->setBinning( *tmp ); // restore binning
                delete tmp;
            } else {
                auto dh = acquire<RooDataHist>(
                        Form("hist_%s_nominal", GetName()), GetTitle(), *x, binningName.c_str() /* binning name*/);
                if (!dh) {
                    Error("create", "something went wrong");
                    return false;
                }
                fComp = acquire<RooHistFunc>(TString::Format("%s_nominal", GetName()), GetTitle(), *x, *dh,
                                             0 /*interpolation order*/);
                dynamic_cast<RooHistFunc *>(fComp.get())->forceNumInt();
            }
            if (kIsFactor || kIsVarFactor) {
                fComp->setAttribute("isFactor"); // so in SetBinContent don't convert to density
            }
            // wrap inside a RooProduct, in case of future interpolations, except if is ParamHistFunc,
            // which we wont interpolate
            if (!kIsVarFactor) {
                fComp = acquire<RooProduct>(GetName(), GetTitle(), *fComp);
                fComp->setStringAttribute("xvar", x->GetName());
                fComp->setStringAttribute("binning", binningName.c_str());
                fComp->SetTitle(hist->GetTitle());
                fComp->setStringAttribute("FillColor", TString::Format("%d", hist->GetFillColor()));
            }
        } else {
            // no histogram, so create either a var or a FlexibleInterpVar instead with nominal value 0
            if (kIsFactor && kIsVarFactor) {
                fComp = acquire<RooRealVar>(GetName(),GetTitle(),1);
            } else {
                fComp = acquire<RooStats::HistFactory::FlexibleInterpVar>((!kIsFactor) ? Form("%s_interp",GetName()):GetName(), GetTitle(), RooArgList(), /*kIsFactor*/ 1 /*nominal always make it 1*/,
                                                                          std::vector<double>(), std::vector<double>());
                if (!kIsFactor) {
                    // wrap in a RooProduct
                    fComp = acquire<RooProduct>(GetName(), GetTitle(), *fComp);
                }
            }
        }
        if (kIsFactor) {
            if (fParent) {
                if(!fParent->Scale(dynamic_cast<RooAbsReal &>(*fComp))) {
                    Warning("Could not scale %s by %s",fParent->GetName(),fComp->GetName());
                }
            }
        } else if (fParent) {
            if(auto p = dynamic_cast<RooProdPdf*>(fParent->get()); p) {
                // need to add to prodpdf ...
                RooRealSumPdf *r = dynamic_cast<RooRealSumPdf *>(fParent->mainPdf());
                if (!r) {
                    // create RooRealSumPdf and add it
                    auto mainPdf = std::dynamic_pointer_cast<RooAbsPdf>(fParent->acquire(
                            std::make_shared<RooRealSumPdf>(Form("%s_pdf", fParent->GetName()),
                                                            fParent->GetTitle(), RooArgList(), RooArgList(),
                                                            true)));
                    if (x) {
                        mainPdf->setStringAttribute("xvar", x->GetName());
                        mainPdf->setStringAttribute("binning", binningName.c_str());
                    }
                    p->_pdfList.add(*mainPdf);
                    p->_pdfNSetList.Add(new RooArgSet("nset"));
                    if (!p->canBeExtended() && mainPdf->canBeExtended()) { p->_extendedIndex = p->_pdfList.size() - 1; }
                    // TODO: any more cleanup?
                    p->_cacheMgr.reset();
                    p->setValueDirty();
                    p->setNormRange(0);
                    r = dynamic_cast<RooRealSumPdf *>(fParent->mainPdf());
                }
                if (!r) {
                    Error("create", "something went wrong!");
                    return false;
                }
                r->_coefList.add(*std::dynamic_pointer_cast<RooRealVar>(
                        acquire(std::make_shared<RooRealVar>("1", "1", 1))));
                r->_funcList.add(*dynamic_cast<RooAbsReal *>(get()));

                // check if there are any scale factors that need auto scaling
                std::set<std::string> doneSF;
                if(fParent->get()->getStringAttribute("factors")) {
                    std::vector<TRegexp> chanPatterns;
                    TStringToken pattern(fParent->get()->getStringAttribute("factors"), ";");
                    while (pattern.NextToken()) {
                        if (pattern.Length()==0) continue;
                        if (!doneSF.count(pattern.Data())) {
                            doneSF.insert(pattern.Data());
                            Info("SetContent","Auto-scaling %s by %s",GetName(),pattern.Data());
                            Scale( *fParent->getObject<RooAbsReal>(pattern.Data()) );
                        }
                        chanPatterns.emplace_back(TRegexp(pattern, true));
                    }
                }


                r->_normIntMgr.reset();
                r->_normMgr.reset();
            }
        }
    }

    if (hist && !(made && hist->GetEntries()==0)) {
        for(int i = 1; i <= hist->GetNbinsX(); i++) {
            if (!SetBinContent(i,hist->GetBinContent(i), varPar, high)) return false;
            if ((!varPar || strlen(varPar)==0) && hist->GetSumw2N()) {
                SetBinError(i,hist->GetBinError(i),"stat");
            }
        }
    }
    return true;

}

bool Component::addConstraint(RooAbsPdf& pdf, bool allowDuplicate) {
    // TODO: acquire pdf?
    auto o = get();
    if (auto p = dynamic_cast<RooProdPdf*>(o); p) {
        if (allowDuplicate || !p->_pdfList.find(pdf)) {
            p->_pdfList.add(pdf);
            p->_pdfNSetList.Add(new RooArgSet("nset"));
            // TODO: any more cleanup?
            p->_cacheMgr.reset();
            p->setValueDirty();
            p->setNormRange(0);
        }
        return true;
    } else if(fParent) {
        return fParent->addConstraint(pdf,allowDuplicate);
    }
    // TODO: if not a RooProdPdf create one ... ??
    return false;
}

bool Component::SetBinContent(int bin, double value, const char* varPar, bool high) {
    int origbin = bin;
    auto o = get();
    if (auto p = dynamic_cast<RooProduct*>(o); p) {
        for(auto c : p->components()) {
            // varFactors are not put inside RooProducts, so if they exist they are factors on this component
            if (dynamic_cast<ParamHistFunc*>(c) || dynamic_cast<RooRealVar*>(c)) continue;
            // TODO: should really require one of the settable types: FlexibleInterpVar, PiecewiseInterpolation,RooHistFunc
            o = c;break;
        }
    }

    if (auto p = dynamic_cast<ParamHistFunc*>(o); p) {
        // modifying a shapeFactor: change the value of the parameter in the corresponding bin
        // if the factor is constrained (i.e. it's a shapeSys) then also modify the constraint
        // accordingly so that the nominal value will be the new value
        // If varPar given then refers to the name of one of the parameters in the constraint
        if( p->_paramSet.size() < bin ) {
            return false;
        }
        o = p->_paramSet.at(bin-1); // will be dealt with below if its settable

        if (strcmp(o->GetName(),"1")==0) {
            TString sVarPar = (varPar) ? varPar : "";
            if ( (sVarPar=="tau" && (value==std::numeric_limits<double>::infinity() || std::isnan(value))) ||
                    (sVarPar=="sigma" && (value==0))){
                // parameter not needed if error is zero
                return true;
            }

            // replace placeholder with a new variable
            RooArgList all;
            for(int i = 0; i < p->_paramSet.getSize();i++) {
                if (i!=bin-1) all.add(*p->_paramSet.at(i));
                else {
                    TString parName = Form("gamma_%s_bin%d",p->GetName(),bin);
                    auto v = acquire<RooRealVar>(parName,parName,1).get();
                    o = v;
                    all.add(*o);

                    if ( (sVarPar=="tau" && value==0) ||
                         (sVarPar=="sigma" && (std::isnan(value)||std::numeric_limits<double>::infinity()==value))){
                        // varFactor parameter is unconstrained ... so ensure its floating with its range
                        if (v->isConstant()) {
                            v->setConstant(false);
                            if (v->getMax()==std::numeric_limits<double>::infinity() || v->getMin()==-std::numeric_limits<double>::infinity()) {
                                v->setRange(0, 5);
                            }
                        }
                        return true;
                    }

                }
            }
            p->_paramSet.removeAll();
            p->_paramSet.add(all);
        }
        if(dynamic_cast<RooRealVar*>(o)) bin = 0; // so that can be dealt with below
    }

    if (auto p = dynamic_cast<RooRealVar*>(o); p) {
        if (bin != 0) {
            Error("SetBinContent","bin must be 0 for variable %s",GetName());
            return false;
        }
        if (!varPar || strlen(varPar)==0) {
            if (p->getMax() < value) p->setMax(value);
            if (p->getMin() > value) p->setMin(value);
            p->setVal(value);
        }

        auto constr = getConstraint(*p);
        if (!constr && varPar && strlen(varPar)) {
            // create constraint term for variable, based on name of varPar:
            // sigma: Gaus(v_obs|v,sigma)
            // tau: Pois(g_obs|v*tau)
            // ...

            TString sVarPar = varPar;
            if (sVarPar=="sigma") {
                if (value==0) {
                    // no constraint needed
                    return true;
                }
                auto glob = acquire<RooRealVar>(Form("gobs_%s",p->GetName()),Form("gobs_%s",p->GetName()),p->getVal(),p->getVal()-5*value,p->getVal()+5*value);
                constr = acquire<RooGaussian>(Form("constraint_%s",p->GetName()),"",
                                              *glob,
                                              *p,*acquire<RooConstVar>(Form("sigma_%s",p->GetName()),"",value)).get();
                addGlobalObservable(*glob );
                p->setRange(p->getVal()-5*value,p->getVal()+5*value);
                p->setError(value);
                addParameter(*p);
            } else if(sVarPar=="tau") {
                if (value==std::numeric_limits<double>::infinity() || std::isnan(value)) {
                    // no constraint needed
                    return true;
                }
                auto glob = acquire<RooRealVar>(TString::Format("gobs_%s",p->GetName()),TString::Format("gobs_%s",p->GetName()),p->getVal()*value);
                auto tau = acquire<RooConstVar>(TString::Format("tau_%s",p->GetName()),"",value);
                constr = acquire<RooPoisson>(TString::Format("constraint_%s",p->GetName()),"",
                                             *glob,
                                             *acquire<RooProduct>(TString::Format("mean_%s",p->GetName()),"", RooArgList(*p,*tau ) ),
                                             true /* no rounding */
                ).get();
                addGlobalObservable(*glob );
                //adjust the range of the stat factor to 5*sqrt(tau)
                p->setRange( std::max((1. - 5.*sqrt(1./tau->getVal())) ,1e-15) , 1. + 5.*sqrt(1./tau->getVal()) );
                p->setError(sqrt(1./tau->getVal()));
                addParameter(*p);
            } else {
                Error("SetBinContent","Unknown constraint parameter %s. Only know: sigma, tau", varPar);
                return false;
            }
            addConstraint(*constr);
        }

        if (constr) {
            RooAbsRealLValue* glob = nullptr;
            RooAbsRealLValue* cpar = nullptr;
            RooConstVar* ccpar = nullptr;

            // find server in the constraint that starts with the varPar ...

            auto allPars = std::make_unique<RooArgSet>();//std::unique_ptr<RooArgSet>( constr->getParameters(RooArgSet()) );
            constr->treeNodeServerList(allPars.get());
            //allPars->Print();constr->Print();
            for(auto par : *allPars) {
                if (par->getAttribute("isGlobalObs")) {
                    if (glob) {
                        Error("SetBinContent","Found multiple global observables in constraint?");
                        return false;
                    }
                    glob = dynamic_cast<RooAbsRealLValue*>(par);
                }
                if (varPar && strlen(varPar) && TString(par->GetName()).BeginsWith(varPar)) {
                    if (cpar) {
                        Error("SetBinContent","Found multiple matched parameters found in constraint? %s",varPar);
                        return false;
                    }
                    cpar = dynamic_cast<RooAbsRealLValue*>(par);
                    if (!cpar) ccpar = dynamic_cast<RooConstVar*>(par); // constvars arent lvalues sadly
                }
            }

            // update constraint parameter
            if (varPar && strlen(varPar)) {
                if (!cpar && !ccpar) {
                    Error("SetBinContent","Could not find settable parameter %s in %s",varPar,constr->GetName());
                    return false;
                }
                TString sVarPar(varPar);
                if (sVarPar.BeginsWith("tau")) {
                    // if value is 0 then just remove the constraint.
                    // if value is infinite then constraint is full, so remove the constraint and set it constant
                    // TODO actually removing the scale factor here ... perhaps shouldn't do this though?
                    if ((value == std::numeric_limits<double>::infinity() || std::isnan(value)) || value<=0) {
                        if(!removeConstraint(*constr)) {
                            Warning("SetBinContent","Unable to remove constraint %s in %s",constr->GetName(),GetName());
                        }
                        if (value!=0) {
                            if (p->getVal()==1) {
                                // unscale
                                if (!Unscale(origbin,*p)) {
                                    Warning("SetBinContent","Could not unscale %s by %s",GetName(),p->GetName());
                                }
                            } else {
                                Warning("SetBinContent","Holding %s constant=%f but leaving it scaling %s",p->GetName(),p->getVal(),GetName());
                                p->setConstant();
                            }
                        }
                        return true;
                    }
                } else if(sVarPar.BeginsWith("sigma")) {
                    if ((value == std::numeric_limits<double>::infinity() || std::isnan(value)) || value<=0) {
                        if(!removeConstraint(*constr)) {
                            Warning("SetBinContent","Unable to remove constraint %s in %s",constr->GetName(),GetName());
                        }
                        if (value==0) {
                            if (p->getVal()==1) {
                                if(!Unscale(origbin, *p)){
                                    Warning("SetBinContent","Could not unscale %s by %s",GetName(),p->GetName());
                                }
                            } else {
                                Warning("SetBinContent","Holding %s constant=%f but leaving it scaling %s",p->GetName(),p->getVal(),GetName());
                                p->setConstant();
                            }
                        }
                        return true;
                    }
                }
/*
                if ( (sVarPar.BeginsWith("tau") && (value == std::numeric_limits<double>::infinity() || std::isnan(value)))
                    || (sVarPar.BeginsWith("sigma") && value == 0)) {
                    // means the parameter is to be removed
                    if (!Unscale(origbin, *p)){
                        Warning("SetBinContent","Could not unscale %s by %s",GetName(),p->GetName());
                    }
                    if(!removeConstraint(*constr)) {
                        Warning("SetBinContent","Unable to remove constraint %s in %s",constr->GetName(),GetName());
                    }
                    return true;
                }
*/

                if (cpar) cpar->setVal(value);
                else if(ccpar) {
                    ccpar->_value = value; // in future ROOT versions there is a changeVal method!
                }
                // also update the parameter range and error

                if (sVarPar.BeginsWith("tau")) {
                    p->setRange(std::max((1. - 5. * sqrt(1. / value)), 1e-15),
                                1. + 5. * sqrt(1. / value));
                    p->setError(sqrt(1. / value));
                } else if(sVarPar.BeginsWith("sigma")) {
                    p->setRange(p->getVal()-5*value,p->getVal()+5*value);
                    p->setError(value);
                } else {
                    Warning("SetBinContent","Do not know how to update %s range and error",p->GetName());
                }
                // also ensure parameter is floating
                p->setConstant(false);

            }

            // now must update the global observable value and our parameter's range
            if (glob) {
                // For RooPoisson and RooGaussian find the first server that isn't glob or cpar
                for (auto s : constr->servers()) {
                    if (auto r = dynamic_cast<RooAbsReal *>(s); r && s != glob && s != cpar && s != ccpar) {
                        if(auto set = ws()->getSnapshot("NominalGlobalObs"); set && set->find(glob->GetName())) {
                            glob->setVal(r->getVal());
                            const_cast<RooArgSet*>(set)->setRealValue(glob->GetName(),glob->getVal());
                            break;
                        }
                    }
                }
            }
        }

        if(ws() && ws()->_snapshots.find("NominalParamValues")) {
            // actually want to set it inside the NominalParamValues snapshot
            if (auto v = dynamic_cast<RooRealVar*>(ws()->getSnapshot("NominalParamValues")->find(p->GetName())); v) {
                v->setError(p->getError());
                v->setVal(p->getVal());
                v->setRange(p->getMin(),p->getMax());
            }
        }

    } else  if (auto p = dynamic_cast<RooStats::HistFactory::FlexibleInterpVar*>(o); p) {
        if (varPar && strlen(varPar)) {
            auto v = acquire<RooRealVar>(varPar,varPar,-5,5 );
            if (!v->hasError()) {
                // assume is first import so create constraint term
                acquire<RooGaussian>(Form("constraint_%s",varPar),"",
                        *acquire<RooRealVar>(Form("gobs_%s",varPar),Form("gobs_%s",varPar),-5,5),
                        *v,RooFit::RooConst(1));
                addGlobalObservable(*getObject<RooRealVar>(Form("gobs_%s",varPar)));
                v->setError(1);
                addParameter(*v);
            }

            if (!p->findServer(*v)) {
                p->_paramList.add(*v);
                p->_low.push_back(0);
                p->_high.push_back(0);
                p->_interpCode.push_back(4);
                v->setAttribute(Form("SYMMETRIC%s_%s",high?"+":"-",GetName())); // flag for symmetrized

                // run through parents, finding first RooProdPdf and adding constraint to it
                if (auto constr = getObject<RooAbsPdf>(Form("constraint_%s",varPar)); constr && !addConstraint(*constr.get())) {
                        Warning("SetBinContent","Could not add constraint");
                }
            }
            if(high) {
                p->setHigh(*v,value);
                if( v->getAttribute(Form("SYMMETRIC+_%s",GetName())) ) {
                    p->setLow(*v, 2*p->_nominal - value);
                }
                v->setAttribute(Form("SYMMETRIC-_%s",GetName()),false);
            } else {
                p->setLow(*v,value);
                if( v->getAttribute(Form("SYMMETRIC-_%s",GetName())) ) {
                    p->setHigh(*v, 2*p->_nominal - value);
                }
                v->setAttribute(Form("SYMMETRIC+_%s",GetName()),false);
            }
        } else {
            p->setNominal(value);
        }
        p->setValueDirty();

    } else if (auto p = dynamic_cast<PiecewiseInterpolation*>(o); p) {
        // find the desired sub-component
        RooHistFunc* f = dynamic_cast<RooHistFunc*>(p->_nominal.absArg());
        RooHistFunc* nomf = f;
        RooHistFunc* otherf = nullptr;
        if(varPar && strlen(varPar)) {
            size_t i = 0;
            for(auto par : p->paramList()) {
                if (strcmp(par->GetName(),varPar)==0) {
                    f = dynamic_cast<RooHistFunc*>((high ? p->highList() : p->lowList()).at(i));
                    otherf = dynamic_cast<RooHistFunc*>((high ? p->lowList() : p->highList()).at(i));
                    break;
                }
                i++;
            }
            if (i==p->paramList().size()) {
                // need to add the parameter
                auto v = acquire<RooRealVar>(varPar,varPar,-5,5 );
                if (!v->hasError()) {
                    // assume is first import so create constraint term
                    acquire<RooGaussian>(Form("constraint_%s",varPar),"",
                                         *acquire<RooRealVar>(Form("gobs_%s",varPar),Form("gobs_%s",varPar),-5,5),
                                         *v,RooFit::RooConst(1));
                    addGlobalObservable(*getObject<RooRealVar>(Form("gobs_%s",varPar)));
                    v->setError(1);
                    addParameter(*v);
                }
                if (auto constr = getObject<RooAbsPdf>(Form("constraint_%s",varPar)); constr && !addConstraint(*constr.get())) {
                    Warning("SetBinContent","Could not add constraint");
                }
                auto up = dynamic_cast<RooHistFunc*>(f->Clone(Form("%s_%s_up",GetName(),varPar)));
                auto down = dynamic_cast<RooHistFunc*>(f->Clone(Form("%s_%s_down",GetName(),varPar)));
                // RooHistFunc doesn't clone it's data hist ... do it ourself
                auto h1 = up->_dataHist = dynamic_cast<RooDataHist*>(f->dataHist().Clone(Form("hist_%s",up->GetName())));
                auto h2 = down->_dataHist = dynamic_cast<RooDataHist*>(f->dataHist().Clone(Form("hist_%s",down->GetName())));

                auto ups = std::dynamic_pointer_cast<RooHistFunc>(acquire(std::shared_ptr<RooHistFunc>(up)));
                auto downs = std::dynamic_pointer_cast<RooHistFunc>(acquire(std::shared_ptr<RooHistFunc>(down)));

                p->_highSet.add(*ups.get());
                p->_lowSet.add(*downs.get());
                p->_interpCode.push_back(4);
                p->_paramSet.add(*v);
                p->setValueDirty();
                f = ((high)?ups:downs).get();
                otherf = ((high)?downs:ups).get();
                // start off with everything being symmetric
                otherf->setAttribute("symmetrized");
                delete h1; delete h2;
            }
        }
        if (f) {
            if (f->dataHist().numEntries() <= bin-1) {
                Error("SetBinContent","%s has %d bins", f->GetName(), f->dataHist().numEntries());
                return false;
            }
            auto bin_pars = f->dataHist().get(bin-1);
            double newValue = value / (f->getAttribute("isFactor") ? 1. : f->dataHist().binVolume(*bin_pars)); // must be a density
            f->dataHist().set(*bin_pars,newValue);
            if(f->getAttribute("symmetrized")) f->setAttribute("symmetrized",false);

            if (otherf && otherf->getAttribute("symmetrized")) {
                // update symmetrically
                otherf->dataHist().set(*bin_pars, 2*nomf->dataHist().get_wgt(bin-1) - newValue);
            }

            f->setValueDirty();
        } else {
            Error("SetBinContent","%s Not Settable", GetName());
            return false;
        }
    } else if (auto p = dynamic_cast<RooHistFunc*>(o); p) {
        if (p->dataHist().numEntries() <= bin-1) {
            Error("SetBinContent","%s has %d bins", p->GetName(), p->dataHist().numEntries());
            return false;
        }

        if (varPar && strlen(varPar)) {
            // need to convert into a PiecewiseInterpolation
            TString n = TString::Format("%s_interp",GetName());
            auto new_p = acquire<PiecewiseInterpolation>(n,GetTitle(),*p,RooArgList(),RooArgList(),RooArgList());
            auto old_p = p;
            new_p->setAttribute(Form("ORIGNAME:%s",old_p->GetName()));
            auto itr =  old_p->clientIterator();
            TObject* obj;
            while( (obj = itr->Next()) ) {
                if( RooAbsArg* arg = dynamic_cast<RooAbsArg*>(obj); arg ) {
                    //arg->replaceServer(*old_p, *new_p, true,true);
                    arg->redirectServers(RooArgSet(*new_p),false,true);
                }
            }
            delete itr;
            return SetBinContent(bin,value,varPar,high);

        }

        auto bin_pars = p->dataHist().get(bin-1);
        p->dataHist().set(*bin_pars,value / (p->getAttribute("isFactor") ? 1. : p->dataHist().binVolume(*bin_pars)));
        p->setValueDirty();
    }  else if(!o && fParent){
        if (kIsVarFactor && (value==std::numeric_limits<double>::infinity()||std::isnan(value))) {
            return true;
        }

        TH1 *h = nullptr;
        if (bin>0) {
            // need to determining the binning
            if (!fParent->GetXaxis()) {
                Error("SetBinContent","Cannot determine the binning for %s",GetName());
                return false;
            }
            if (fParent->GetXaxis()->IsVariableBinSize()) {
                h = new TH1D(GetTitle(), GetTitle(), fParent->GetXaxis()->GetNbins(),
                             fParent->GetXaxis()->GetXbins()->GetArray());
            } else {
                h = new TH1D(GetTitle(), GetTitle(), fParent->GetXaxis()->GetNbins(), fParent->GetXaxis()->GetXmin(),
                             fParent->GetXaxis()->GetXmax());
            }
            h->Sumw2();
            h->GetXaxis()->SetName(
                    Form("%s;%s", fParent->GetXaxis()->GetParent()->GetName(), fParent->GetXaxis()->GetName()));
            if (!kIsVarFactor) h->SetBinContent(bin, value); // leave hist blank to create ParamHistFunc with all empty
            else {
                // to create unconstrained shapeFactor (i.e. shape varFactor) must set all errors to infinite
                for(int i=1;i<=h->GetNbinsX();i++) {
                    h->SetBinError(i, std::numeric_limits<double>::infinity());
                }
            }
        }

        auto out = SetContent(h, varPar, high);
        if (h) {
            delete h;
            if(!kIsVarFactor) return out; // SetContent takes care of setting values
        }
        // here then created either a FlexibleInterpVar or a RooRealVar or a ParamHistFunc but still need to set value
        return SetBinContent(bin,value,varPar,high);
        /*
        if (bin>0 && fParent->GetXaxis()) {
            // try to 'create' the histfunc or paramhistfunc (if its a factor
            delete h;
            return out;
        } else if (auto p = dynamic_cast<RooProduct*>(fParent->get()); p && bin<=0) {
            // create a flexible interpvar and add it to the product
            fComp = acquire<RooStats::HistFactory::FlexibleInterpVar>(GetName(),GetTitle(), RooArgList(), 1, std::vector<double>{}, std::vector<double>{});
            fParent->Scale(*dynamic_cast<RooAbsReal*>(fComp.get()));
            return SetBinContent(bin,value,varPar,high); // should set it above
        }
         */
    } else {
        Error("SetBinContent","%s Not Settable", GetName());
        return false;
    }

    /*
 *  turns out this doesn't help integral get re-calculated but is still necessary
 *  because some integrals may be made from old state ... TODO: is there a better way to flush all caches?
 *  actually seems need to do setDirtyInhibit still
 *  see e.g. SetBinContent, then GetBinContent, then set a variation, move the variation par, and then getVal
 *  the getVal will have the wrong denominator (see Print) ... inhibiting dirty will make it eval correctly.
 */
    // recursive through all clients and sterlize their normalization caches
    std::function<void(RooAbsArg*)> func;
    func = [&func](RooAbsArg* a) {
        if (!a) return;
        auto itr =  a->clientIterator();
        TObject* obj;
        while( (obj = itr->Next()) ) {
            if( RooAbsPdf* arg = dynamic_cast<RooAbsPdf*>(obj); arg ) {
                arg->_normMgr.reset();
                if (RooProdPdf* p = dynamic_cast<RooProdPdf*>(arg); p) {
                    p->_cacheMgr.reset();
                    p->setNormRange(0);
                }
                //std::cout << "cleaned " << arg->GetName() << std::endl;

            }
            if( RooAbsArg* arg = dynamic_cast<RooAbsArg*>(obj); arg ) {
                func(arg);
            }
        }
        delete itr;
    };
    func(get());
    return true;

    /*
    auto f = dynamic_cast<RooHistFunc*>(o);
    RooHistFunc* otherf = nullptr; // TODO
    RooHistFunc* nomf = nullptr; // TODO

    if (!f) {
        if (!o && fParent && fParent->GetXaxis()) {
            // try to 'create' the histfunc

            TH1* h = nullptr;
            if(fParent->GetXaxis()->IsVariableBinSize()) {
                h = new TH1D(GetTitle(),GetTitle(),fParent->GetXaxis()->GetNbins(),fParent->GetXaxis()->GetXbins()->GetArray());
            } else {
                h = new TH1D(GetTitle(),GetTitle(),fParent->GetXaxis()->GetNbins(),fParent->GetXaxis()->GetXmin(),fParent->GetXaxis()->GetXmax());
            }

            h->GetXaxis()->SetName(Form("%s;%s", fParent->GetXaxis()->GetParent()->GetName(), fParent->GetXaxis()->GetName()));
            h->SetBinContent(bin,value);
            auto out = SetContent(h);
            delete h;
            return out;
        }
        return false;
    }
    if (f->dataHist().numEntries() <= bin-1) {
        Error("SetBinContent","%s has %d bins", f->GetName(), f->dataHist().numEntries());
        return false;
    }
    auto bin_pars = f->dataHist().get(bin-1);
    double newValue = value / f->dataHist().binVolume(*bin_pars); // must be a density
    f->dataHist().set(*bin_pars,newValue);
    if(f->getAttribute("symmetrized")) f->setAttribute("symmetrized",false);

    if (otherf && otherf->getAttribute("symmetrized")) {
        // update symmetrically
        otherf->dataHist().set(*bin_pars, 2*nomf->dataHist().get_wgt(bin-1) - newValue);
    }

    f->setValueDirty();
    return true;
     */
}

void Component::addGlobalObservable(RooAbsLValue& obs) {
    // add to NominalGlobalObs snapshot
    dynamic_cast<RooAbsArg&>(obs).setAttribute("Constant",true);
    dynamic_cast<RooAbsArg&>(obs).setAttribute("isGlobalObs",true);
    if(auto t = dynamic_cast<RooArgSet*>(ws()->_snapshots.find("NominalGlobalObs")); t) {
        if (t->find(dynamic_cast<RooAbsArg&>(obs))) return;
        const_cast<RooArgSet*>(t)->addClone(dynamic_cast<RooAbsArg&>(obs));
    } else {
        RooArgSet pars;
        pars.add(dynamic_cast<RooAbsArg&>(obs));
        ws()->saveSnapshot("NominalGlobalObs",pars);
    }
}

void Component::addParameter(RooAbsLValue& obs) {
    // add to NominalParamValues snapshot - ensure is floating
    dynamic_cast<RooAbsArg&>(obs).setAttribute("Constant",false);
    if(auto t = dynamic_cast<RooArgSet*>(ws()->_snapshots.find("NominalParamValues")); t) {
        if (t->find(dynamic_cast<RooAbsArg&>(obs))) return;
        const_cast<RooArgSet*>(t)->addClone(dynamic_cast<RooAbsArg&>(obs));
    } else {
        RooArgSet pars;
        pars.add(dynamic_cast<RooAbsArg&>(obs));
        ws()->saveSnapshot("NominalParamValues",pars);
    }
}

// varPar here is the varFactor name
// or if calling this directly on a varFactor then specify the constraint par name
bool Component::SetBinError(int bin, double value, const char* varPar, bool high) {
    double origValue = value;
    TString sVarPar = (varPar && strlen(varPar)) ? varPar : "";

    Component vf = (kIsVarFactor) ? *this : varFactor(sVarPar.Data());
    TString constraintPar;
    if (kIsVarFactor) {
        // defaulting to poisson constraints for all varFactors ...
        if(sVarPar=="") constraintPar = "tau";
        else constraintPar = sVarPar;
    } else {
        if (!vf.get() && sVarPar=="") {
            // will create special statFactor using the channel name
            vf.SetName(Form("%s_", fParent->GetName()));
            if (value != 0 && vf.get() && !scaleFactors().find(*vf.get())) {
                // need to scale this sample by the special common factor
                Scale(*dynamic_cast<RooAbsReal *>(vf.get()));
            }
        }
        constraintPar = (sVarPar == "" || sVarPar == "stat" || sVarPar == "shape") ? "tau" : "sigma";
    }


    double nomValue = 0, new_sumw=0, new_sumw2=0;
    if (constraintPar == "tau") {
        // tau needs to be equal to (sumw)^2 / sumw2
        // we assume the value given is the w2 of this sample
        // sumw is the nominal value of either this sample or all the samples that utilise this stat factor


        auto o = get();
        if (auto p = dynamic_cast<RooProduct*>(o); p) {
            for(auto c : p->components()) {
                if (dynamic_cast<ParamHistFunc*>(c)) continue;
                o = c;break;
            }
        }
        if (auto p = dynamic_cast<ParamHistFunc*>(o); p) {
            nomValue = dynamic_cast<RooAbsReal *>(p->_paramSet.at(bin))->getVal();
            // or here should we be accessing the parent nominal value?
        } else if (auto p = dynamic_cast<RooStats::HistFactory::FlexibleInterpVar*>(o); p) {
            nomValue = p->_nominal;
        } else if(auto p = dynamic_cast<PiecewiseInterpolation*>(o); p) {
            // find the desired sub-component
            RooHistFunc *f = dynamic_cast<RooHistFunc *>(p->_nominal.absArg());
            auto bin_pars = f->dataHist().get(bin - 1);
            nomValue = f->dataHist().weight() * (f->getAttribute("isFactor") ? 1. : f->dataHist().binVolume(*bin_pars));
        } else if(auto f = dynamic_cast<RooHistFunc*>(o); f) {
            auto bin_pars = f->dataHist().get(bin - 1);
            nomValue = f->dataHist().weight() * (f->getAttribute("isFactor") ? 1. : f->dataHist().binVolume(*bin_pars));
        }

        if (!kIsVarFactor && sVarPar == "" && vf.get()) {
            auto par = dynamic_cast<ParamHistFunc*>(vf.get())->_paramSet.at(bin-1);
            new_sumw = double(TString(par->getStringAttribute("sumw")).Atof())-double(TString(par->getStringAttribute(Form("sumw_%s",GetName()))).Atof()) + nomValue;
            new_sumw2 = double(TString(par->getStringAttribute("sumw2")).Atof())-double(TString(par->getStringAttribute(Form("sumw2_%s",GetName()))).Atof()) + pow(value,2);
            value = pow( new_sumw, 2) / ( new_sumw2 );
        } else {
            value = pow(nomValue,2) / pow(value,2);
        }

    }

    auto res =  vf.SetBinContent(bin,value,constraintPar,high);

    if (sVarPar == "" && res && vf.get()) {
        // update the parameter values
        auto par = dynamic_cast<ParamHistFunc*>(vf.get())->_paramSet.at(bin-1);
        par->setStringAttribute("sumw", TString::Format("%f",new_sumw) );
        par->setStringAttribute("sumw2", TString::Format("%f", new_sumw2));
        par->setStringAttribute(Form("sumw_%s",GetName()), TString::Format("%f",nomValue));
        par->setStringAttribute(Form("sumw2_%s",GetName()), TString::Format("%f",origValue*origValue));

    }
    return res;
//
//    // need the nominal value ...
//    double nominalValue = 0;
//    auto o = get();
//    if (auto p = dynamic_cast<RooProduct*>(o); p) {
//        for(auto c : p->components()) {
//            if (dynamic_cast<ParamHistFunc*>(c)) continue;
//            o = c;break;
//        }
//    }
//    if (auto p = dynamic_cast<ParamHistFunc*>(o); p) {
//        nominalValue = dynamic_cast<RooAbsReal*>(p->_paramSet.at(bin))->getVal();
//    }else if (auto p = dynamic_cast<RooStats::HistFactory::FlexibleInterpVar*>(o); p) {
//        nominalValue = p->_nominal;
//    } else if(auto p = dynamic_cast<PiecewiseInterpolation*>(o); p) {
//        // find the desired sub-component
//        RooHistFunc *f = dynamic_cast<RooHistFunc *>(p->_nominal.absArg());
//        auto bin_pars = f->dataHist().get(bin - 1);
//        nominalValue = f->dataHist().weight() * (f->getAttribute("isFactor") ? 1. : f->dataHist().binVolume(*bin_pars));
//    } else if(auto f = dynamic_cast<RooHistFunc*>(o); f) {
//        auto bin_pars = f->dataHist().get(bin - 1);
//        nominalValue = f->dataHist().weight() * (f->getAttribute("isFactor") ? 1. : f->dataHist().binVolume(*bin_pars));
//    }
//
//
//    if(varPar && strlen(varPar) && strcmp(varPar,"stat")) {
//        return SetBinContent(bin, nominalValue + ((high) ? value : -value), varPar, high);
//    }
//
//    // if here, setting the stat error term, which is represented by a shapeFactor
//    auto oo = dynamic_cast<RooProduct*>(get());
//    if (!oo) {
//        // TODO: inject a RooProduct at this location in the tree
//        if(value) Error("SetBinError","%s is not settable",GetName());
//        return (value==0);
//    }
//
//    auto sfs = scaleFactors(bin - 1);
//
//    RooRealVar* gamma = nullptr;
//    RooPoisson* gamma_constraint = nullptr;
//    RooAbsReal* tau = nullptr;
//    RooRealVar* gobs = nullptr;
//    for(auto sf : sfs) {
//        gamma = dynamic_cast<RooRealVar*>(sf);
//        if (!gamma) continue;
//        if(gamma_constraint = dynamic_cast<RooPoisson*>(getConstraint(*gamma)); gamma_constraint) {
//            break;
//        }
//        gamma = nullptr;
//    }
//    if (gamma_constraint) {
//        // adjust the value of gamma tau term
//        RooArgList mean_pars = dynamic_cast<RooProduct*>( gamma_constraint->mean.absArg() )->_compRSet;
//        mean_pars.remove(*gamma);
//        assert(mean_pars.size()==1);
//
//        tau = dynamic_cast<RooAbsReal*>(mean_pars.first());
//        gobs = dynamic_cast<RooRealVar*>(gamma_constraint->x.absArg());
//
//        if (value==0) {
//            //Info("SetBinError","removing %s",gamma_constraint->GetName());
//            // TODO: Check is gamma is a shared scale factor! .. if it is, don't remove it but adjust it's values
//            // actually removing!
//            removeConstraint(*gamma_constraint);
//            // remove the gamma scale factor
//            Unscale(bin, *gamma);
//            return false;
//        }
//
//    } else if(value==0) {
//        // no error given and none existing so nothing to do
//        //Info("SetBinError","No Error for %s bin %d",GetName(),bin);
//        return false;
//    } else {
//        // TODO: Look for gamma in other samples - only if sharing errors
//
//
//        // do we need to create a new gamma? perhaps already exists ...
//        TString parName = TString::Format("gamma_%s_bin%d",GetName(),bin);
//        if (getObject<RooAbsPdf>(TString::Format("constraint_%s",parName.Data()))) {
//            //Info("SetBinError","Reusing existing constraint %s_constraint", parName.Data());
//            gamma = getObject<RooRealVar>(parName.Data()).get();
//            tau = getObject<RooAbsReal>(parName + "_tau").get();
//            gobs = getObject<RooRealVar>(Form("gobs_%s",parName.Data())).get();
//            if (!scaleFactors(bin - 1).find(*gamma)) {
//                Scale(bin, *gamma);
//            }
//        } else {
//
//            if (nominalValue == 0) {
//                SetBinContent(bin, 1e-15);
//                nominalValue = 1e-15;
//            }
//            gobs = acquire<RooRealVar>(TString::Format("gobs_%s",parName.Data()),TString::Format("gobs_%s",parName.Data()),1).get();
//            gamma = acquire<RooRealVar>(parName,parName,1,1e-15,5).get();
//            tau = acquire<RooConstVar>(parName+"_tau","",0).get();
//            acquire<RooPoisson>(TString::Format("constraint_%s",parName.Data()),"",
//                    *gobs,
//                            *acquire<RooProduct>(parName+"_mean","",
//                                    RooArgList(*gamma,*tau )),
//                                    true);
//            tau->setStringAttribute("sumw2", "0");
//            addGlobalObservable(*gobs);
//            addParameter(*gamma);
//            Scale(bin, *gamma);
//        }
//
//        addConstraint(*getObject<RooAbsPdf>(TString::Format("constraint_%s",parName.Data())));
//
//    }
//
//    double sumw2;
//    if (auto x = tau->getStringAttribute("sumw2"); x) {
//        sumw2 = TString(x).Atof();
//    } else {
//        Error("SetBinError","Tau parameter not tracked, cannot set stat error");
//        return false;
//    }
//    double old_sumw2 = 0;
//    if (auto x = tau->getStringAttribute(Form("sumw2_%s",GetName())); x) {
//        old_sumw2 = TString(x).Atof();
//    }
//
//    // need tau = (sumw)^2/sumw2
//
//    sumw2 += pow(value,2) - old_sumw2;
//    if (sumw2==0) {
//        std::cout << "GOT 0 : " << value << " " << old_sumw2 << " " << GetName() << " " << bin << std::endl;
//    }
//    if (auto c = dynamic_cast<RooConstVar*>(tau); c) {
//        c->_value = pow(nominalValue,2)/sumw2; // in future ROOT versions there is a changeVal method!
//    } else if(auto c = dynamic_cast<RooRealVar*>(tau); c) {
//        c->setVal(pow(nominalValue,2)/sumw2);
//    } else {
//        Error("SetBinError","Cannot set %s",tau->GetName());
//        return false;
//    }
//    tau->setStringAttribute("sumw",Form("%f",nominalValue));
//    tau->setStringAttribute("sumw2",Form("%f",sumw2));
//    tau->setStringAttribute(Form("sumw2_%s",GetName()),Form("%f",pow(value,2)));
//    //adjust the range of the stat factor to 5*sqrt(tau)
//    gamma->setRange( std::max((1. - 5.*sqrt(1./tau->getVal())) ,1e-15) , 1. + 5.*sqrt(1./tau->getVal()) );
//    // and set the gobs to value of tau
//    if(gobs && ws()) {
//        // actually want to set it inside the NominalGlobalObs snapshot
//        const_cast<RooArgSet*>(ws()->getSnapshot("NominalGlobalObs"))->setRealValue(gobs->GetName(),tau->getVal());
//    }
//
//    gamma->setError(sqrt(1./tau->getVal()));
//
//    if(ws()) {
//        // actually want to set it inside the NominalParamValues snapshot
//        dynamic_cast<RooRealVar*>(const_cast<RooArgSet*>(ws()->getSnapshot("NominalParamValues"))->find(gamma->GetName()))->setError(gamma->getError());
//    }
//
//    return true;

}

RooAbsPdf* Component::getConstraint() const {
    auto o = dynamic_cast<RooAbsReal*>(get());
    if (!o) return nullptr;
    return getConstraint(*o);
}

RooAbsPdf* Component::getConstraint(const RooAbsReal& par) const {
    auto o = dynamic_cast<RooProdPdf*>(get());
    if (!o) {
        if (!fParent) {
            // if at top-level and is a simultaneous, check all channels for a constraint
            for(auto& c : channels()) {
                if(auto out = c.second.getConstraint(par); out) {
                    return out;
                }
            }
            return nullptr;
        }
        return fParent->getConstraint(par);
    }
    for(auto p : o->pdfList()) {
        if (p==mainPdf()) continue;
        if (p->dependsOn(par)) return dynamic_cast<RooAbsPdf*>(p);
    }
    return nullptr;
}

bool Component::removeConstraint(const RooAbsPdf& constraintPdf) {

    auto o = get();
    // TODO: if not a RooProdPdf create one ...
    if (auto p = dynamic_cast<RooProdPdf*>(o); p) {
        auto i = p->_pdfList.index(constraintPdf);
        if (i>=0) {
            p->_pdfList.remove(constraintPdf);
            auto nset = p->_pdfNSetList.At(i);
            p->_pdfNSetList.Remove(nset);
            delete nset; // I don't think the RooLinkedList owned it so must delete ourself
            // TODO: any more cleanup?
            p->_cacheMgr.reset();
            p->setValueDirty();
            p->setNormRange(0);
        }
    } else {
        if (!fParent) return false;
        return fParent->removeConstraint(constraintPdf);
    }

    const RooAbsPdf* pdf = &constraintPdf;
    TString s(pdf->getStringAttribute("channels"));
    if (s.Length()>0) {
        s.ReplaceAll(GetName(),"");
        s.ReplaceAll(";;",";");
        const_cast<RooAbsPdf&>(*pdf).setStringAttribute("channels",s);
    }
    return true;
}

bool Component::Scale(const std::string& name) {
    if (ws() && ws()->function(name.c_str())) {
        return Scale( *ws()->function(name.c_str()) );
    }
    return false;
}

bool Component::Scale(const Component& c) {
    auto s = dynamic_cast<RooAbsReal*>(c.get());
    if (s) { return Scale( *s ); }
    return false;
}

bool Component::Scale(RooAbsReal& func) {
    bool hasSample = false;
    for(auto& s : samples()) {
        hasSample = true;
        s.Scale(func);
    }
    if (hasSample) {
        // record that this channel was scaled by this factor so when new samples added they get auto-scaled
        get()->setStringAttribute("factors",TString::Format("%s%s;",TString(get()->getStringAttribute("factors")).Data(),func.GetName()));
        return true;
    }
    auto p = dynamic_cast<RooProduct*>(get());
    if (!p) return hasSample;
    p->_compRSet.add(func);
    p->setValueDirty();
    // TODO: check if func is constrained - if it is, need to add constraint term to channel ProdPdf
    return true;
}

std::vector<Component> Component::samples() const {
    std::vector<Component> out;
    if(auto s = dynamic_cast<RooRealSumPdf*>(get()); s) {
        for(auto& p : s->funcList()) {
            out.push_back(Component(*p,*this));
        }
    } else if(auto p = mainPdf(); p && p != get()) {
        for(auto& c : Component(*p,*this).samples()) {
            out.push_back(Component(*c.get(),*this));
        }
    }
    return out;
}



Component Component::shallowCopy(const std::string& name) {
    auto o = get();
    if (!o) { return Component("",ws()); }
    auto chans = channels();
    if (!chans.empty()) {
        // create a new RooSimultaneous with shallow copies of each channel
        std::shared_ptr<RooSimultaneous> pdf;
        pdf = acquire<RooSimultaneous>(name.c_str(),o->GetTitle(),*cat());
        for(auto& c : chans) {
            auto c_copy = c.second.shallowCopy(name+"_"+c.first);
            // FIXME: Need to prevent channel pdf from being destroyed if not owned by WS
            pdf->addPdf(*dynamic_cast<RooAbsPdf*>(c_copy.get()),c.first.c_str());
        }
        return Component(name,ws(),nullptr,fData,pdf);
    }

    if(auto p = dynamic_cast<RooProdPdf*>(o);p) {
        // main pdf will be copied too
        std::shared_ptr<RooProdPdf> pdf = std::dynamic_pointer_cast<RooProdPdf>(acquire( std::shared_ptr<TObject>(p->Clone(name.c_str()) ) ) ); // use clone to copy all attributes etc too
        auto main = mainPdf();
        if (main) {
            auto newMain = std::dynamic_pointer_cast<RooAbsArg>( acquire(std::shared_ptr<TObject>(main->Clone( (name + "_pdf").c_str()))) );
            pdf->replaceServer(*pdf->_pdfList.find(main->GetName()),*ws()->arg((name+"_pdf").c_str()),true,true);
            pdf->_pdfList.replace(*pdf->_pdfList.find(main->GetName()),*ws()->arg((name+"_pdf").c_str()));
            pdf->_cacheMgr.reset();
            pdf->setValueDirty();
            pdf->setNormRange(0);
        }
        return Component(name,ws(),nullptr,fData,pdf);
    }

    return Component("",ws());

}


THStack* Component::BuildStack(const FitResult& fr) {

    // stacks don't need bin errors so can make a copy of fr without errors
    FitResult frNoError("");
    if(fr->_finalPars) {
        frNoError.reset(fr->_finalPars);
        for (auto r : frNoError->floatParsFinal()) {
            if (auto v = dynamic_cast<RooRealVar *>(r);v) {
                v->setError(0);
            }
        }
    }
    auto out = new THStack(GetName(),TString::Format("%s;%s;Events",GetTitle(),GetTitle()));

    // first check if any of the samples have fill colors. If none do then we will generate some for them
    bool noColors=true;
    for(auto& s : samples()) {
        if (!s.get()) continue;
        if (s->getStringAttribute("FillColor")) { noColors=false; break; }
    }

    int count = 2;
    for(Component& s : samples()) {
        if(noColors && s.get()) {
            s.get()->setStringAttribute("FillColor",Form("%d",count++));
        }
        auto h = s.BuildHistogram(frNoError);
        h->SetBinError(1,0);
        out->Add( h );
    }

    if (auto ax = GetXaxis(); ax) {
        out->SetTitle(TString::Format("%s;%s;Events",GetTitle(),ax->GetTitle()));
    }

    return out;
}

TLegend* Component::getLegend(bool create) {
    if (auto p = dynamic_cast<TLegend*>(gPad->GetPrimitive("legend")); p) return p;
    // look for a parent pad called 'legend' and create it there if existing
    auto p = gPad;
    while( (p != p->GetMother()) && (p = p->GetMother()) ) {
        if (auto q = dynamic_cast<TVirtualPad*>(p->GetPrimitive("legend")); q) {
            q->Modified();
            p = q; break;
        }
    }
    auto tmpPad = gPad;
    TLegend* l = nullptr;
    if (p && strcmp(p->GetName(),"legend")==0) {
        if (l = dynamic_cast<TLegend*>(p->GetPrimitive("legend")); l || !create) return l;
        p->cd();
        l = new TLegend(gPad->GetLeftMargin(), 1. - gPad->GetTopMargin(), 1. - gPad->GetRightMargin(),
                        gPad->GetBottomMargin());
    } else {
        if (!create) return nullptr;
        l = new TLegend(0.5, 1. - gPad->GetTopMargin() - 0.1, 1. - gPad->GetRightMargin(),
                        1. - gPad->GetTopMargin() - 0.03);
        l->SetBorderSize(0);
    }
    l->SetBit(kCanDelete);
    //l->SetMargin(0);
    l->SetFillStyle(0);
    l->SetName("legend");
    l->Draw();
    l->ConvertNDCtoPad();
    tmpPad->cd();
    return l;
};

#include "TLegendEntry.h"

void Component::addLegendEntry(TObject* o, const char* title, const char* opt) {
    auto l = Component::getLegend();
    if (!l) return;
    // check for entry already existing with same title
    for(auto a : *l->GetListOfPrimitives()) {
        if(!strcmp(dynamic_cast<TLegendEntry*>(a)->GetLabel(),title)) return;
    }

    l->AddEntry(o,title,opt);
    if(auto nObj = l->GetListOfPrimitives()->GetEntries();nObj>1) {
        double newY1 = l->GetY1NDC() - (l->GetY2NDC()-l->GetY1NDC())/(std::ceil(double(nObj-1)/l->GetNColumns()));
        if (newY1 < 0.5) {
            // create new columns instead
            l->SetNColumns(l->GetNColumns()+1);
        } else {
            l->SetY1NDC(newY1);
        }
    }
    getLegend(); // to mark modified
}

void Component::Draw(Option_t* opt, const FitResult& fr) {
    TString sOpt(opt);
    bool isYield=sOpt.Contains("yield");
    if (isYield) { sOpt.ReplaceAll("yield",""); }
    bool norename = sOpt.Contains("norename");
    if (norename) { sOpt.ReplaceAll("norename",""); }

    FitResult myFr = fr;

    if (!fr->_finalPars && get()) {
        // assume they wanted all pars
        auto s = std::unique_ptr<RooArgSet>(get()->getParameters(*obs()));
        s->remove( *std::unique_ptr<RooAbsCollection>(s->selectByAttrib("Constant",true)) );
        RooArgList l(*s);
        myFr.reset( &l  );
        //s->Print();
    } else if(get()) {
        // augment the fitresult finalPars with non-const parameters that are not present in either the finalPars
        // or the constantPars

    }

    TVirtualPad *pad = gPad;

    if (pad && (!pad->GetListOfPrimitives() || pad->GetListOfPrimitives()->GetEntries()==0)) {
        sOpt.ReplaceAll("same","");
    }

    auto chans = channels();

    if (!sOpt.Contains("same")) {
        if (!pad) {
            TCanvas::MakeDefCanvas(); // TODO: When I tried to use MakeDefConvas I got warnings??
            pad = gPad;
        }
        pad->Clear();
        if (!chans.empty() && !isYield) {
            dynamic_cast<TPad *>(pad)->DivideSquare(chans.size(),1e-12,1e-12);
            int i = 1;
            for (auto& [k,v] : chans) {
                pad->GetPad(i)->SetName(k.c_str());
                i++;
            }
        }
        pad->cd(0);
        if(!norename) pad->SetName(GetName());
    }




    if (!chans.empty()) {
        // if doing a yield plot, use cat var as x-axis
        if (isYield) {
            TH1D *yieldHist = new TH1D(GetName(), GetTitle(), chans.size(), 0, chans.size());
            yieldHist->Sumw2();
            yieldHist->GetXaxis()->SetTitle(cat()->GetTitle());
            yieldHist->GetYaxis()->SetTitle("Events / Channel");
            yieldHist->SetDirectory(0);
            yieldHist->SetBit(kCanDelete);
            std::map<TString,TH1D*> samps;
            int i = 1;
            for (auto&[k, v] : chans) {
                yieldHist->GetXaxis()->SetBinLabel(i, k.c_str());
                yieldHist->SetBinContent(i, v.Integral(myFr));
                //yieldHist->SetBinError(i,yieldHist->GetBinContent(i)*0.1);
                i++;
            }
            i=1;
            for (auto&[k,v] : chans) {
                for(auto& s : v.samples()) {
                    TString sName = s.GetTitle(); // group by title!
                    if (!samps.count(sName)) {
                        samps[sName] = (TH1D*)yieldHist->Clone(sName);
                        samps[sName]->Reset();
                        samps[sName]->SetTitle(s.GetTitle());
                        samps[sName]->SetFillColor(TString(s->getStringAttribute("FillColor")).Atoi());
                    }
                    samps[sName]->SetBinContent(i,s.Integral(myFr));
                }
                i++;
            }
            THStack* yieldStack = new THStack(GetName(),TString::Format("%s;%s;Events",GetTitle(),yieldHist->GetTitle()));
            for(auto& [c,v] : samps) {
                yieldStack->Add(v);
            }
            yieldStack->SetBit(kCanDelete);
            yieldHist->Draw("axis");
            yieldStack->Draw("noclear same");

            addLegendEntry(yieldHist,"Uncertainty", "fl"); // TODO - no error on integral yet

            TList *ll = yieldStack->GetHists();
            for (int i = ll->GetEntries() - 1; i >= 0; i--) { //go in reverse order
                addLegendEntry(ll->At(i), ll->At(i)->GetTitle(), "f");
            }
            // note: labels arent displayed correctly if axis name is not 'xaxis' ... need to work around this
            auto y = (TH1D*)yieldHist->Clone("axis");
            y->SetBit(kCanDelete);
            y->GetXaxis()->SetName(cat()->GetName()); // needed to plot e.g. data graph
            y->Draw("axissame");

            yieldHist->SetFillStyle(3005);
            yieldHist->SetFillColor(yieldHist->GetLineColor());
            yieldHist->SetMarkerStyle(0);
            yieldHist->Draw(sOpt+"e2same");
        } else {
            // go through current pad's subpads and each pad with channel name we will draw onto
            if (!pad->GetListOfPrimitives()) return; // no subpads to draw channels onto
            for (auto o : *pad->GetListOfPrimitives()) {
                if (!o->InheritsFrom(TPad::Class())) continue;
                dynamic_cast<TPad *>(o)->cd();
                for (auto&[k, v] : chans) {
                    if (k == o->GetName()) {
                        if (myFr.fModel && !myFr.fModel->channels().count(k)) {
                            gPad->SetFillColor(kGray);
                        }
                        v.Draw(sOpt + "norename", myFr);
                        break;
                    }
                }
            }
        }
    } else {

        if (sOpt.Contains("signif")) {
            sOpt.ReplaceAll("signif","");
            // requested aux plot ... divide pad again
            pad->Divide(1,2,1e-12,1e-12);
            double padFrac = 0.3;
            pad->GetPad(1)->SetName("Events");
            pad->GetPad(2)->SetName("Significance");
            pad->GetPad(2)->SetPad(0,0,1,padFrac);
            pad->GetPad(1)->SetPad(0,padFrac,1,1);
            pad->GetPad(2)->SetTopMargin(0.03);
            pad->GetPad(1)->SetBottomMargin(0);
            //pad->GetPad(1)->SetTopMargin(pad->GetPad(1)->GetTopMargin()*(1.-padFrac)/padFrac);
            pad->GetPad(2)->SetBottomMargin(pad->GetPad(2)->GetBottomMargin()*(1.-padFrac)/padFrac);
        }

        // if has a "main" subpad then will draw onto that
        bool doneDraw = false;
        if(auto p = dynamic_cast<TVirtualPad*>(gPad->FindObject("Events")); p) {
            p->cd();
            Draw(sOpt,myFr);
            gPad->SetName("Events"); // will have renamed so restore the name
            doneDraw = true;
            pad->cd();
        }
        if(auto p = dynamic_cast<TVirtualPad*>(gPad->FindObject("Significance")); p) {
            p->cd();
            if (!sOpt.Contains("same")) {
                auto ax = GetXaxis();
                TH1 *h = nullptr;
                if (!ax) {
                    h = new TH1D(GetName(), GetTitle(), 1, 0, 1);
                } else {
                    h = new TH1D(GetName(), GetTitle(), ax->GetNbins(), dynamic_cast<Axis *>(ax)->binning()->array());
                    h->GetXaxis()->SetName(ax->GetParent()->GetName());
                    h->GetXaxis()->SetTitle(ax->GetTitle());
                }
                h->SetDirectory(0);
                h->GetYaxis()->SetTitle("signif. / #sigma");
                h->SetMinimum(-3);
                h->SetMaximum(3);
                h->SetLineWidth(0);
                h->SetBit(kCanDelete);

                h->GetYaxis()->SetNdivisions(3,0,0);
                double rHeight = (1. - (gPad->GetHNDC()))/(gPad->GetHNDC());
                h->GetYaxis()->SetTitleSize( h->GetYaxis()->GetTitleSize() * rHeight );
                h->GetYaxis()->SetLabelSize( h->GetYaxis()->GetLabelSize() * rHeight);
                h->GetXaxis()->SetTitleSize( h->GetXaxis()->GetTitleSize() * rHeight );
                h->GetXaxis()->SetLabelSize( h->GetXaxis()->GetLabelSize() * rHeight );
                h->GetYaxis()->SetTitleOffset( h->GetYaxis()->GetTitleOffset() / rHeight);
                h->SetStats(false);h->SetBit(TH1::kNoTitle);
                h->Draw();
                gPad->SetGridy();
            }
            doneDraw = true;
            pad->cd();
        }

        if (!doneDraw) {
            auto hist = BuildHistogram(myFr);
            hist->SetDirectory(0);
            hist->SetBit(kCanDelete); // so that will be deleted if pad is cleared
            hist->Draw("axis");
            TLatex l;l.SetNDC();l.SetTextAlign(12);
            l.DrawLatex(0.4,1. - gPad->GetTopMargin()/2.,hist->GetTitle());
            //((THistPainter*)((TH1*)hist)->GetPainter())->PaintTitle();

            if (!samples().empty() && !sOpt.Contains("same")) {
                auto stack = BuildStack(myFr);
                stack->SetBit(kCanDelete);
                stack->Draw("noclear same");
                addLegendEntry(hist, "Uncertainty", "fl");

                TList *ll = stack->GetHists();
                for (int i = ll->GetEntries() - 1; i >= 0; i--) { //go in reverse order
                   addLegendEntry(ll->At(i), ll->At(i)->GetTitle(), "f");
                }
                // adjust range so that every bit of first entry in stack is visible
                double ymin, ymax;
                ymax = std::max(stack->GetHists()->Last() ? ((TH1*)stack->GetHists()->Last())->GetMaximum() : 0, hist->GetMaximum() )* 1.1;
                ymin = hist->GetMinimum();
                if (TH1 *firstHist = dynamic_cast<TH1 *>(stack->GetHists()->At(0)); firstHist) {
                    for (int i = 0; i < firstHist->GetNbinsX(); i++) {
                        ymin = std::min(ymin, firstHist->GetBinContent(i - 1) * 0.9);
                    }
                }
                hist->SetAxisRange(ymin, ymax, "Y");
            }
            hist->Draw("axissame"); //gPad->RedrawAxis(); <-- alternative?
            hist->SetFillStyle(3005);
            hist->SetFillColor(hist->GetLineColor());
            hist->SetMarkerStyle(0);
            hist->Draw("e2same");
            if (fParent) {
                for (auto &c : fParent->channels()) {
                    if (strcmp(c.second.GetName(), GetName()) == 0) {
                        gPad->SetName(c.first.c_str()); // name is used above to flag it as a 'significance' pad etc
                        break;
                    }
                }
            }
        }
    }

    pad->cd();
    pad->Modified();
    pad->Update();

}

Data Component::data(const std::string& name) const {
    Data out(name,fWS);
    if (GetXaxis()) { out.fXaxis = fXaxis; }
    // if we are in a channel, get the cat value of this channel
    if (fParent && fParent->cat()) {
        for(auto c : fParent->channels()) {
            if (strcmp( c.second.GetName(), GetName())==0) {
                fParent->cat()->setLabel(c.first);
                RooArgSet s(*fParent->cat());
                out.fSnap.reset( s.snapshot() );
            }
        }
    }
    return out;
}