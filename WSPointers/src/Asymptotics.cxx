#include "WSPointers/Asymptotics.h"

#include <cmath>
#include "Math/ProbFunc.h"
#include "Math/BrentRootFinder.h"
#include "Math/WrappedFunction.h"

Double_t Asymptotics::k(PLLType compatFunc, double pValue, double poiVal, double poiPrimeVal, double sigma, double low, double high) {

    // determine the pll value corresponding to nSigma expected - i.e. where the altPValue equals e.g. 50% for nSigma=0,
    //find the solution (wrt x) of: FitManager::altPValue(x, var(poi), alt_val, _sigma_mu, _compatibilityFunction) - targetPValue = 0
    double targetTailIntegral = pValue; //ROOT::Math::normal_cdf(*nSigma);

    // check how much of the alt distribution density is in the delta function @ 0
    // if more than 1 - target is in there, if so then pll must be 0
    double prob_in_delta = Phi_m(poiVal, poiPrimeVal, std::numeric_limits<double>::infinity(), sigma, compatFunc);
    // also get a contribution to the delta function for mu_hat < mu_L IF mu==mu_L
    if (poiVal == low) {
        // since mu_hat is gaussian distributed about mu_prime with std-dev = sigma
        // the integral is Phi( mu_L - mu_prime / (sigma) )
        double mu_L = low;
        prob_in_delta += ROOT::Math::normal_cdf((mu_L - poiPrimeVal) / sigma);
    }

    if (prob_in_delta > 1-targetTailIntegral) {
        return 0;
    }

    struct TailIntegralFunction {
        TailIntegralFunction(double _poiVal, double _alt_val, double _sigma_mu, double _low, double _high,
                             PLLType _compatibilityFunction, double _target) :
                poiVal(_poiVal), alt_val(_alt_val), sigma_mu(_sigma_mu), low(_low),high(_high), target(_target),
                cFunc(_compatibilityFunction) {}

        double operator()(double x) const {
            return PValue(cFunc, x, poiVal, alt_val, sigma_mu,low,high) - target;
        }

        double poiVal, alt_val, sigma_mu, low,high,target;
        PLLType cFunc;
    };

    TailIntegralFunction f(poiVal, poiPrimeVal, sigma, low, high, compatFunc, targetTailIntegral);
    ROOT::Math::BrentRootFinder brf;
    ROOT::Math::WrappedFunction<TailIntegralFunction> wf(f);

    auto tmpLvl = gErrorIgnoreLevel;
    gErrorIgnoreLevel = kFatal;
    double _pll = 500.;
    double currVal(1.);
    int tryCount(0);
    double _prev_pll = _pll;
    do {
        currVal = wf(_pll);
        if (currVal > 1e-4) _pll = 2. * (_pll + 1.); // goto bigger pll scale
        else if (currVal < -1e-4) _pll /= 2.; // goto smaller pll scale
        //std::cout << "pll = " << _pll << " currVal = " << currVal << std::endl;
        brf.SetFunction(wf, 0, _pll);
        if (brf.Solve()) {
            _prev_pll = _pll;
            _pll = brf.Root();
        }
        //std::cout << " -- " << brf.Root() << " " << FitManager::altPValue(_pll, mu, alt_val, sigma, pllModifier()) << " >> " << wf(_pll) << std::endl;
        tryCount++;
        if (tryCount > 20) {
            gErrorIgnoreLevel = tmpLvl;
            Warning("Asymptotics::pll", "Reached limit pValue=%g pll=%g", pValue, _pll);
            break;
        }
    } while (std::abs(wf(_pll)) > 1e-4 && std::abs(wf(_pll)) < std::abs(wf(_prev_pll)) * 0.99);
    gErrorIgnoreLevel = tmpLvl;
    return _pll;

}

Double_t Asymptotics::PValue(PLLType compatFunc, double k, double poiVal, double poi_primeVal, double sigma, double lowBound, double upBound) {
    //uncapped test statistic is equal to onesidednegative when k is positive, and equal to 1.0 - difference between twosided and onesidednegative when k is negative ...
    if(compatFunc==Uncapped) {
        //if(k==0) return 0.5;
        if(k>0) return PValue(OneSidedNegative,k,poiVal,poi_primeVal,sigma,lowBound,upBound);
        return 1. - (PValue(TwoSided,-k,poiVal,poi_primeVal,sigma,lowBound,upBound) - PValue(OneSidedNegative,-k,poiVal,poi_primeVal,sigma,lowBound,upBound));
    }


    //if(k<0) return 1.;
    if(k<=0) {
        if(compatFunc==OneSidedNegative && std::abs(poiVal-poi_primeVal)<1e-9) return 0.5; //when doing discovery (one-sided negative) use a 0.5 pValue
        return 1.; //case to catch the delta function that ends up at exactly 0 for the one-sided tests
    }

    if(sigma==0) {
        if (lowBound!=-std::numeric_limits<double>::infinity() || upBound != std::numeric_limits<double>::infinity()) {
            return -1;
        } else if(std::abs(poiVal-poi_primeVal)>1e-12) {
            return -1;
        }
    }

    //get the poi value that defines the test statistic, and the poi_prime hypothesis we are testing
    //when setting limits, these are often the same value

    Double_t Lambda_y = 0;
    if(std::abs(poiVal-poi_primeVal)>1e-12) Lambda_y = (poiVal-poi_primeVal)/sigma;

    Double_t k_low = (lowBound == -std::numeric_limits<double>::infinity()) ? std::numeric_limits<double>::infinity() : pow((poiVal - lowBound)/sigma,2);
    Double_t k_high = (upBound == std::numeric_limits<double>::infinity()) ? std::numeric_limits<double>::infinity() : pow((upBound - poiVal)/sigma,2);

    double out = Phi_m(poiVal,poi_primeVal,std::numeric_limits<double>::infinity(),sigma,compatFunc) - 1;

    if (out < -1) {
        // compatibility function is unsupported, return negative
        return -2;
    }

    //go through the 4 'regions' ... only two of which will apply
    if( k <= k_high ) {
        out += ROOT::Math::gaussian_cdf(sqrt(k)+Lambda_y) - Phi_m(poiVal,poi_primeVal,Lambda_y + sqrt(k),sigma,compatFunc);
    } else {
        double Lambda_high = (poiVal - upBound)*(poiVal + upBound - 2.*poi_primeVal)/(sigma*sigma);
        double sigma_high = 2.*(upBound-poiVal)/sigma;
        out +=  ROOT::Math::gaussian_cdf((k-Lambda_high)/sigma_high) - Phi_m(poiVal,poi_primeVal,(k - Lambda_high)/sigma_high,sigma,compatFunc);
    }

    if( k <= k_low ) {
        out += ROOT::Math::gaussian_cdf(sqrt(k)-Lambda_y) + Phi_m(poiVal,poi_primeVal,Lambda_y - sqrt(k),sigma,compatFunc);
    } else {
        double Lambda_low = (poiVal - lowBound)*(poiVal + lowBound - 2.*poi_primeVal)/(sigma*sigma);
        double sigma_low = 2.*(poiVal - lowBound)/sigma;
        out +=  ROOT::Math::gaussian_cdf((k-Lambda_low)/sigma_low) + Phi_m(poiVal,poi_primeVal,(Lambda_low - k)/sigma_low,sigma,compatFunc);
        /*out +=  ROOT::Math::gaussian_cdf((k-Lambda_low)/sigma_low) +
             2*Phi_m(poiVal,poi_primeVal,(Lambda_low - k_low)==0 ? 0 : ((Lambda_low - k_low)/sigma_low),sigma,compatFunc)
             - Phi_m(poiVal,poi_primeVal,(Lambda_low - k)/sigma_low,sigma,compatFunc);
*/

        // handle case where poiVal = lowBound (e.g. testing mu=0 when lower bound is mu=0).
        // sigma_low will be 0 and gaussian_cdf will end up being 1, but we need it to converge instead
        // to 0.5 so that pValue(k=0) converges to 1 rather than 0.5.
        // handle this by 'adding' back in the lower bound
        // TODO: Think more about this?
        /*if (sigma_low == 0) {
            out -= 0.5;
        }*/


    }

    return 1. - out;
}



Double_t Asymptotics::Phi_m(double mu, double mu_prime, double a, double sigma, Asymptotics::PLLType compatFunc ) {
    //implemented only for special cases of compatibility function
    if(compatFunc==TwoSided) {
        return 0;
    }

    if (sigma==0) sigma=1e-100;// avoid nans if sigma is 0

    if(compatFunc==OneSidedPositive) {
        //ignore region below x*sigma+mu_prime = mu ... x = (mu - mu_prime)/sigma
        if(a < (mu-mu_prime)/sigma) return 0;
        return ROOT::Math::gaussian_cdf(a) - ROOT::Math::gaussian_cdf((mu-mu_prime)/sigma);
    } else if(compatFunc==OneSidedNegative) {
        //ignore region above x*sigma+mu_prime = mu ...
        if(a > (mu-mu_prime)/sigma) return ROOT::Math::gaussian_cdf((mu-mu_prime)/sigma);
        return ROOT::Math::gaussian_cdf(a);
    } else if(compatFunc==OneSidedAbsolute) {
        //cutoff at x*sigma+mu_prime = mu for mu<0 , and turns back on at x*sigma+mu_prime=mu for mu>0
        //ignore region between x = (-mu-mu_prime)/sigma and x = (mu-mu_prime)/sigma
        double edge1 = (-mu - mu_prime)/sigma;
        double edge2 = (mu-mu_prime)/sigma;

        if(edge1>edge2) { double tmp=edge1; edge1=edge2; edge2=tmp; }

        if(a<edge1) return ROOT::Math::gaussian_cdf(a);
        if(a<edge2) return ROOT::Math::gaussian_cdf(edge1);
        return ROOT::Math::gaussian_cdf(a) - (ROOT::Math::gaussian_cdf(edge2) - ROOT::Math::gaussian_cdf(edge1));
    }
    //msg().Error("Phi_m","Unknown compatibility function ... can't evaluate");
    return -1;
}
