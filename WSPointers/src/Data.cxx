
#define private public
#include "RooWorkspace.h"
#undef private

#include "WSPointers/Data.h"

#include "RooAbsData.h"
#include "WSPointers/Component.h"
#include "TPad.h"

#include "RooAbsReal.h"
#include "TGraphAsymmErrors.h"
#include "TLegend.h"
#include "Math/SpecFuncMathCore.h"
#include "TMath.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooRealVar.h"

Data::Data(const std::string& name, RooWorkspace* ws) : TNamed(name,""), fWS(ws ? ws : &Component::space()) {
    TString dName(GetName());
    if (!dName.Contains(";")) {
        // must have a matching snapshot otherwise will default to NominalParamValues for gobs
        if(!fWS->_snapshots.find(dName)) {
            if(!fWS->_snapshots.find("NominalGlobalObs") && fWS->_snapshots.find("NominalParamValues")) {
                fWS->saveSnapshot("NominalGlobalObs",*std::unique_ptr<RooArgSet>((RooArgSet*)fWS->getSnapshot("NominalParamValues")->selectByAttrib("Constant",true)));
            }
            if(fWS->_snapshots.find("NominalGlobalObs")) {
                SetName(dName + ";NominalGlobalObs");
            }
        }
    }
}

Component Data::comp(const std::string& name) {
    return Component(name,fWS,nullptr,std::make_shared<Data>(*this));
}

Data::Data(RooAbsData* data, const RooArgSet* gobs) : TNamed("",""), fData(data), fGobs(gobs) {

}

RooAbsData* Data::get() const {
    if (strlen(GetName())==0) return nullptr;
    if (!fData && fWS) {
        TString dName(GetName());
        if (dName.Contains(";")) dName = dName(0,dName.Index(";"));
        if (auto d = fWS->data(dName); d) {
            fData.reset(d, [](RooAbsData*){});
        }
    }
    return fData.get();
}

const RooArgSet* Data::gobs() const {
    if (strlen(GetName())==0) return nullptr;
    if (!fGobs && fWS) {
        TString dName(GetName());
        if (dName.Contains(";")) dName = dName(dName.Index(";")+1,dName.Length());
        if (auto d = (RooArgSet*)fWS->_snapshots.find(dName); d) {
            fGobs.reset(d, [](const RooArgSet*){});
        }
    }
    return fGobs.get();
}

void Data::Draw(Option_t* opt) {
    TString sOpt(opt);

    if (!sOpt.Contains("same")) {
        return; // TODO
    }

    if (!gPad) return;
    auto pad = gPad;

    auto theData = get();

    // all subpads that have an axis defined we will create graphs for if we can
    for(auto o : *pad->GetListOfPrimitives()) {
        if (o->InheritsFrom(TVirtualPad::Class())) {
            // draw in this pad ..
            ((TVirtualPad*)o)->cd();
            this->Draw(sOpt);
            pad->cd();
        } else if(o->InheritsFrom(TH1::Class()) && (!pad->GetPrimitive("axis") || strcmp(o->GetName(),"axis")==0)) {
            // found an axis ...
            if (auto x = theData->get()->find(((TH1*)o)->GetXaxis()->GetName()); x) {
                TH1* h = (TH1*)o;
                TH1* theHist = (TH1*)(o)->Clone();
                theHist->Reset();
                //this hist will get filled with w*x to track weighted x position per bin
                TH1* xPos = (TH1*)theHist->Clone("xPos");
                xPos->Reset();
                TH1* xPos2 = (TH1*)theHist->Clone("xPos2");
                xPos2->Reset();


                const RooAbsReal* xvar = (x) ? dynamic_cast<RooAbsReal*>(x) : nullptr;
                const RooAbsCategory* xcat = (x && !xvar) ? dynamic_cast<RooAbsCategory*>(x) : nullptr;

                // look for a catvar that has this pad's name (or mothers name if in a special subpad)
                TString catLabel = gPad->GetName();
                if (catLabel=="Events" || catLabel=="Significance") {
                    catLabel = gPad->GetMother()->GetName(); // could also use GetTitle here
                }
                const RooCategory* catvar = cat(catLabel.Data());
                int catIndex = (catvar) ? catvar->lookupIndex(catLabel.Data()) : -1;

                int nevent = theData->numEntries();
                for(int i=0;i<nevent;i++) {
                    theData->get(i);
                    if(catvar && catvar->getIndex() != catIndex) continue;

                    if (xvar){
                        xPos->Fill( xvar->getVal() , xvar->getVal()*theData->weight() );
                        xPos2->Fill( xvar->getVal() , pow( xvar->getVal(), 2 )*theData->weight() );
                    }

                    if (xcat) {
                        theHist->Fill(xcat->getLabel(),theData->weight());
                    } else {
                        theHist->Fill((x) ? xvar->getVal() : 0.5, theData->weight());
                    }
                }

                xPos->Divide(theHist);xPos2->Divide(theHist);


                auto dataGraph = new TGraphAsymmErrors;
                dataGraph->SetName(Form("%s_%s",catLabel.Data(),GetName()));dataGraph->SetTitle(GetTitle());
                *static_cast<TAttMarker*>(dataGraph) = *static_cast<TAttMarker*>(theHist);
                *static_cast<TAttLine*>(dataGraph) = *static_cast<TAttLine*>(theHist);

                dataGraph->SetMarkerStyle(20);dataGraph->SetLineColor(kBlack);

                dataGraph->SetBit(kCanDelete);
                //update the x positions to the means for each bin and use poisson asymmetric errors for data ..
                for(int i=0;i<theHist->GetNbinsX();i++) {
                    if(theHist->GetBinContent(i+1)) {
                        double val = theHist->GetBinContent(i+1);

                        dataGraph->SetPoint(dataGraph->GetN(), (xvar) ? xPos->GetBinContent(i + 1) : theHist->GetBinCenter(i+1), val);

                        //x-error will be the (weighted) standard deviation of the x values ...
                        double xErr = sqrt( xPos2->GetBinContent(i+1) - pow(xPos->GetBinContent(i+1),2) );

                        dataGraph->SetPointError(dataGraph->GetN()-1, xErr, xErr, val - 0.5*TMath::ChisquareQuantile(TMath::Prob(1,1)/2.,2.*(val)), 0.5*TMath::ChisquareQuantile(1.-TMath::Prob(1,1)/2.,2.*(val+1)) - val );
                    }
                }


                delete xPos;
                delete xPos2;

                dataGraph->SetBit(kCanDelete); // will be be deleted when pad is cleared




                if(strcmp(gPad->GetName(),"Significance")==0) {
                    auto signif = [](double n, double b, double sigma) {
                        double t0 = 0;
                        if(sigma<=0.) {
                            //use simplified expression ...
                            t0 = 2.*( ((n==0)?0:n*log(n/b)) - (n-b) );
                        } else {
                            double sigma2 = sigma*sigma;
                            double b_hathat = 0.5*( b - sigma2 + sqrt( pow(b-sigma2,2) + 4*n*sigma2 ) );
                            //double s_hat = n - m;
                            //double b_hat = m;
                            t0 = 2.*( ((n==0)?0:n*log(n/b_hathat)) + b_hathat - n + pow(b-b_hathat,2)/(2.*sigma2) );
                        }
                        return (n>=b) ? sqrt(t0) : -sqrt(t0);
                    };

                    // drawing significance not events -- need to get the expected events ...
                    auto predHist = dynamic_cast<TH1*>(dynamic_cast<TVirtualPad*>(gPad->GetMother()->GetPrimitive("Events"))->GetPrimitive(h->GetName()));
                    if (!predHist) {
                        Error("Draw","Cannot plot significance because cannot find events histogram for %s",h->GetName());
                        return;
                    } else {
                        for(int i=0;i<theHist->GetNbinsX();i++) {
                            dataGraph->SetPoint(i, dataGraph->GetPointX(i), signif(dataGraph->GetPointY(i),predHist->GetBinContent(i+1),predHist->GetBinError(i+1)));
                            dataGraph->SetPointError(i,0,0,0,0);
                            theHist->SetBinContent(i+1,dataGraph->GetPointY(i));
                        }
                        theHist->SetBit(kCanDelete); // will be be deleted when pad is cleared
                        theHist->SetLineWidth(2);
                        theHist->Draw("hist same");
                    }
                } else {
                    delete theHist;
                    dataGraph->Draw("z0p same");
                }


                Component::addLegendEntry(dataGraph,dataGraph->GetTitle(),"pEX0");


                TGraphAsymmErrors* g = dataGraph;

                double ymin = h->GetMinimum(); double ymax = h->GetMaximum();
                bool change=false;
                for(int i = 0; i < g->GetN();i++) {
                    ymax = std::max(ymax, g->GetPointY(i) + g->GetErrorYhigh(i));
                    ymin = std::min(ymin, g->GetPointY(i) - g->GetErrorYlow(i));
                    change=true;
                }
                if(change) h->SetAxisRange(ymin,ymax,"Y");

                break;
            }


        }
    }

    pad->Modified();
    pad->Update();

}

RooCategory* Data::cat(const std::string& label) const {
    if (!get()) return nullptr;
    RooCategory* catvar = nullptr;
    for(auto v : *get()->get()) {
        catvar = dynamic_cast<RooCategory *>(v);
        if (!catvar) continue;
        if (catvar->hasLabel(label.c_str())) {
            break;
        }
        catvar = nullptr;
    }
    if (!catvar) return nullptr;
    return catvar;
}

double Data::Integral() const {
    auto _data = get();
    if (!_data) return std::numeric_limits<double>::quiet_NaN();
    TString catLabel = TNamed::GetTitle(); // TODO : store this elsewhere?
    if (catLabel.Length()==0) {
        return _data->sumEntries();
    }
    // in a subchannel, find the category that has the matching label
    const RooCategory* catvar = cat(catLabel.Data());
    if (!catvar) return 0;
    int idx = catvar->lookupIndex(catLabel.Data());

    // works but generates annoying info msg so just do manually for now
    //auto cut = TString::Format("%s==%s::%s",catvar->GetName(),catvar->GetName(),catLabel.Data());
    //return _data->sumEntries(cut);

    double out = 0;
    for(int i =0;i<_data->numEntries();i++) {
        _data->get(i);
        if (catvar->getIndex()!=idx) continue;
        out += _data->weight();
    }
    return out;
}

Data Data::reduce(const char* cut) const {
    Data out((get()) ? get()->reduce(cut) : nullptr);
    out.fGobs = fGobs; // so wont take ownership if its a shared gobs
    out.SetName(TNamed::GetName());
    out.fWS = fWS;
    return out;
}

Data Data::channel(const std::string& channel) const {
    Data out(*this);
    out.TNamed::SetTitle(channel.c_str()); // TODO: Better place to store this
    return out;
}

bool Data::SetBinContent(int bin, double value) {
    if (!fXaxis) return false;

    RooArgSet obs;
    dynamic_cast<RooAbsLValue*>(fXaxis->GetParent())->setBin(bin-1,fXaxis->GetName());
    obs.add(*dynamic_cast<RooAbsArg*>(fXaxis->GetParent()));
    if (fSnap) obs.add(*fSnap);

    if (!get()) {
        // must be stored in a workspace if we are setting content
        if (!ws()) {
            Error("SetBinContent","Modifiable datasets must live in a workspace");
            return false;
        }
        if (!ws()->var("weightVar")) {
            ws()->import(RooRealVar("weightVar","weightVar",1));
        }
        obs.add(*ws()->var("weightVar"));
        TString dName(GetName());
        if (dName.Contains(";")) dName = dName(0,dName.Index(";"));
        RooDataSet s(dName,strlen(GetTitle()) ? GetTitle() : dName.Data(),obs,"weightVar");
        ws()->import(s);
    }

    // need to remove any entries within the range of this bin
    if (auto d = dynamic_cast<RooDataSet*>(get()); d) {

        TString cut = "";

        if (fSnap) {
            for (auto s : *fSnap) {
                if (auto c = dynamic_cast<RooCategory*>(s); c) {
                    cut += TString::Format("%s%s==%d", (cut=="")?"":" && ",c->GetName(), c->getCurrentIndex());
                }
            }
        }
        cut += TString::Format("%s%s >= %f && %s < %f", (cut=="")?"":" && ",fXaxis->GetParent()->GetName(),
                               fXaxis->GetBinLowEdge(bin), fXaxis->GetParent()->GetName(),
                               fXaxis->GetBinUpEdge(bin));


        if (fData->sumEntries(cut) > 0) {
            auto _reduced = fData->reduce(TString::Format("!(%s)", cut.Data()));
            fData->reset();
            for (int j = 0; j < _reduced->numEntries(); j++) {
                auto _obs = _reduced->get(j);
                fData->add(*_obs, _reduced->weight());
            }
        }
    }
    fData->add(obs,value);
    return true;
}

double Data::GetBinContent(int bin) const {
    if (auto d = dynamic_cast<RooDataSet*>(get()); d && fXaxis) {

        TString cut = "";

        if (fSnap) {
            for (auto s : *fSnap) {
                if (auto c = dynamic_cast<RooCategory*>(s); c) {
                    cut += TString::Format("%s%s==%d", (cut=="")?"":" && ",c->GetName(), c->getCurrentIndex());
                }
            }
        }
        cut += TString::Format("%s%s >= %f && %s < %f", (cut=="")?"":" && ",fXaxis->GetParent()->GetName(),
                               fXaxis->GetBinLowEdge(bin), fXaxis->GetParent()->GetName(),
                               fXaxis->GetBinUpEdge(bin));


        return fData->sumEntries(cut);
    }
    return 0;
}

bool Data::Add(TH1* hist) {
    for(int i=1;i<=hist->GetNbinsX();i++) {
        double v = GetBinContent(i);
        if (std::isnan(v)) {
            return false;
        }
        if (!SetBinContent(i, v + hist->GetBinContent(i))) return false;
    }
    return true;
}