
#include "WSPointers/Fitting.h"

#include "RooMinimizer.h"
#include "RooMsgService.h"

#define protected public
#include "RooFitResult.h"
#undef protected

#include "RooRealVar.h"

#include "Math/GenAlgoOptions.h"
#include "TROOT.h"

TObject* Fitting::m_msgObj = 0;

Fitting::FitConfig* Fitting::s_defaultConfig = nullptr;

Fitting::FitConfig& Fitting::defaultConfig() {
    if (s_defaultConfig) return *s_defaultConfig;
    s_defaultConfig = new FitConfig();
    s_defaultConfig->fitConfig.SetParabErrors(true); // will use to run hesse after fit
    s_defaultConfig->fitConfig.MinimizerOptions().SetMinimizerType("Minuit2");
    s_defaultConfig->fitConfig.MinimizerOptions().SetErrorDef(0.5); // ensures errors are +/- 1 sigma ..IMPORTANT
    s_defaultConfig->fitConfig.MinimizerOptions().SetMaxFunctionCalls(9500);
    s_defaultConfig->fitConfig.MinimizerOptions().SetMaxIterations(9500);
    s_defaultConfig->fitConfig.MinimizerOptions().SetStrategy(0);
    s_defaultConfig->fitConfig.MinimizerOptions().SetPrintLevel(-1);
    s_defaultConfig->fitConfig.MinimizerOptions().SetExtraOptions(ROOT::Math::GenAlgoOptions());
    // have to const cast to set extra options
    const_cast<ROOT::Math::IOptions *>(s_defaultConfig->fitConfig.MinimizerOptions().ExtraOptions())->SetValue(
            "StrategySequence", "012");
    // default nll construction options
    s_defaultConfig->nllOpts.Add( RooFit::Offset(true).Clone() );

    if (s_defaultConfig->rootVersion != gROOT->GetVersion()) {
        s_defaultConfig->rootVersion = gROOT->GetVersion();
        s_defaultConfig->uuid = "";
    }
    
    return *s_defaultConfig;
}

RooFitResult* Fitting::minimize(RooAbsReal* func, FitConfig* fc) {

    auto _nll = func;
    if (!_nll) return nullptr;


    bool save = true;
    std::unique_ptr<RooArgSet> allPars(func->getParameters(RooArgSet()));
    std::unique_ptr<RooAbsCollection> floatPars(allPars->selectByAttrib("Constant",kFALSE));
    std::unique_ptr<RooAbsCollection> constPars( allPars->selectByAttrib("Constant",kTRUE) );

    if (!fc) fc = &defaultConfig();
    auto& fitConfig = fc->fitConfig;

    std::string s;
    if (fitConfig.MinimizerOptions().ExtraOptions() ) {
        fitConfig.MinimizerOptions().ExtraOptions()->GetNamedValue("StrategySequence", s);
    }
    TString m_strategy = s;

    int printLevel  =   fitConfig.MinimizerOptions().PrintLevel();
    int strategy =      fitConfig.MinimizerOptions().Strategy();
    //Note: AsymptoticCalculator enforces not less than 1 on tolerance - should we do so too?

    RooFit::MsgLevel msglevel = RooMsgService::instance().globalKillBelow();
    if(printLevel < 0) RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);

    //check how many parameters we have ... if 0 parameters then we wont run a fit, we just evaluate nll and return ...
    if(floatPars->getSize()==0 || fitConfig.MinimizerOptions().MaxFunctionCalls()==0) {
        RooFitResult* result = 0;
        if(save) {
            RooArgList parsList;parsList.add(*floatPars);
            //construct an empty fit result ...
            result = new RooFitResult(); // if put name here fitresult gets added to dir, we don't want that
            result->SetName(TUUID().AsString()); result->SetTitle("No fit");
            result->setFinalParList( parsList );
            result->setInitParList( parsList );
            result->setConstParList(  *allPars ); /* RooFitResult takes a snapshot of allPars */
            TMatrixDSym d;
            result->setCovarianceMatrix( d );
            result->setCovQual(-1);
            result->setMinNLL( _nll->getVal() );
            result->setEDM(0);
            result->setStatus(fitConfig.MinimizerOptions().MaxFunctionCalls()==0);
        }
        if(printLevel < 0) RooMsgService::instance().setGlobalKillBelow(msglevel);
        return result;
    }


    RooMinimizer _minimizer(*_nll);
    _minimizer.fitter()->Config() = fitConfig;
    _minimizer.optimizeConst(2);

    bool hesse = _minimizer.fitter()->Config().ParabErrors();
    _minimizer.fitter()->Config().SetParabErrors(false); // turn "off" so can run hesse as a separate step, appearing in status
    bool restore = !_minimizer.fitter()->Config().UpdateAfterFit();
    _minimizer.fitter()->Config().SetUpdateAfterFit(true); // note: seems to always take effect

    std::vector<TString> algNames;

    //gCurrentSampler = this;
    //gOldHandlerr = signal(SIGINT,toyInterruptHandlerr);

    int status = 0;
    for(int tries=1,maxtries=4; tries <= maxtries; ++tries) {
        TString minim = _minimizer.fitter()->Config().MinimizerType();
        TString algo = _minimizer.fitter()->Config().MinimizerAlgoType();
        status = _minimizer.minimize(minim,algo);
        minim = _minimizer.fitter()->Config().MinimizerType(); // may have changed value
        if(save) algNames.push_back(_minimizer.fitter()->Config().MinimizerType()
                                    + _minimizer.fitter()->Config().MinimizerAlgoType() + std::to_string(_minimizer.fitter()->Config().MinimizerOptions().Strategy()));
        //int status = _minimizer->migrad();
        if(status%1000 == 0) break; //fit was good
        if(tries >= maxtries) break; //giving up

        //NOTE: minuit2 seems to distort the tolerance in a weird way, so that tol becomes 100 times smaller than specified
        //Also note that if fits are failing because of edm over max, it can be a good idea to activate the Offset option when building nll
        Warning("minimize","Fit Status=%d (edm=%f, tol=%f, strat=%d), Rescanning #%d...",status,_minimizer.fitter()->Result().Edm(),
                _minimizer.fitter()->Config().MinimizerOptions().Tolerance(),
                _minimizer.fitter()->Config().MinimizerOptions().Strategy(),tries);
        _minimizer.minimize(minim,"Scan");
        if(save) algNames.push_back(_minimizer.fitter()->Config().MinimizerType()+"Scan");
        if(tries == 2) { //up the strategy (if we can)
            int idx = m_strategy.Index('0'+strategy);
            if(idx!=-1 && idx!=m_strategy.Length()-1) {
                strategy = int(m_strategy(idx+1) - '0');
                _minimizer.setStrategy(strategy);
                tries--; //stick on tries=2 state
            } else {
                tries++; //move on to next
            }
        }
        if(tries == 3) { _minimizer.fitter()->Config().SetMinimizer("Minuit","migradImproved"); }
    }

    if(hesse) _minimizer.hesse(); //note: I have seen that you can get 'full covariance quality' without running hesse ... is that expected?

    //signal(SIGINT,gOldHandlerr);

    RooFitResult* out = _minimizer.save("fitResult","fitResult");
    if (out->status()>0) {
        Warning("minimize","Final fit status is %d",out->status());
    }
    //before returning we will override _minLL with the actual NLL value ... offsetting could have messed up the value
    out->setMinNLL(_nll->getVal());

    // check if any of the parameters are at their limits (potentially a problem with fit)
    // or their errors go over their limits (just a warning)
    RooFIter itr = floatPars->fwdIterator();
    RooAbsArg* a = 0;
    int limit_status = 0;
    std::string listpars;
    while( (a = itr.next()) ) {
        RooRealVar* v = dynamic_cast<RooRealVar*>(a);
        if (!v) continue;
        double vRange = v->getMax() - v->getMin();
        if (v->getMin() > v->getVal()-vRange*0.0001 || v->getMax()< v->getVal()+vRange*0.0001) {
            // within 0.01% of edge

            // check if nll actually lower 'at' the boundary, if it is, refine the best fit to the limit value
            auto tmp = v->getVal();
            v->setVal( v->getMin() );
            double boundary_nll = _nll->getVal();
            if (boundary_nll <= out->minNll()) {
                ((RooRealVar*)out->_finalPars->find(v->GetName()))->setVal(v->getMin());
                out->setMinNLL(boundary_nll);
                //Info("fit","Corrected %s onto minimum @ %g",v->GetName(),v->getMin());
            } else {
                // not better, so restore value
                v->setVal( tmp );
            }

            // if has a 'physical' range specified, don't warn if near the limit
            if (v->hasRange("physical"))
                limit_status = 900;
            listpars += v->GetName(); listpars += ",";
        } else if(hesse && (v->getMin() > v->getVal()-v->getError() || v->getMax() < v->getVal()
                                                                                     +v->getError())) {
            if (printLevel>=0) {
                Info("minimize", "PARLIM: %s (%f +/- %f) range (%f - %f)", v->GetName(), v->getVal(), v->getError(),
                     v->getMin(), v->getMax());
            }
            limit_status = 9000;
        }
    }
    if (limit_status == 900) {
        if (printLevel>=0)
        Warning("miminize","PARLIM: Parameters within 0.001%% limit in fit result: %s",
                listpars.c_str());
    } else if(limit_status > 0) {
        if (printLevel>=0)
        Warning("miminize","PARLIM: Parameters near limit in fit result");
    }



    if (save) {
        //modify the statusHistory to use the algnames instead ..
        int i=0; for(auto& s : algNames) { out->_statusHistory[i++].first=s; }
        // store the limit check result
        out->_statusHistory.push_back(std::make_pair("PARLIM", limit_status ));
        out->_status += limit_status;

    }

    if(status%1000 != 0) {
        Warning("minimize"," fit status %d",status);
    }

    if(printLevel < 0) RooMsgService::instance().setGlobalKillBelow(msglevel);

    // save the nllOpts, fit configuration and result to the store

    // minimizer may have slightly altered the fitConfig (e.g. unavailable minimizer etc) so update for that ...
    if (fc->uuid.empty() && algNames.size()==1) {
        fitConfig.MinimizerOptions() = _minimizer.fitter()->Config().MinimizerOptions();
    }

    out->SetName( TUUID().AsString() );

    if (restore) {
        *floatPars = out->floatParsInit();
    }

    return out;

}