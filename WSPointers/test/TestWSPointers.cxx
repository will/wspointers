#include <gtest/gtest.h>

#include "WSPointers/Component.h"
#include "WSPointers/ComponentCollection.h"
#include "WSPointers/FitResult.h"
#include "RooFitResult.h"
#include "Math/PdfFunc.h"
#include "RooRealVar.h"


TEST(Component, test1) {

    auto w = Component::currentSpace = new RooWorkspace();
    w->factory("mu[1,-1,5]");

    Component c("mu");
    ASSERT_EQ( c.get(), w->arg("mu") );
    ASSERT_EQ( Component("another").get(), nullptr );


}

TEST(Component, test2) {

    //auto w = Component::currentSpace = new RooWorkspace();
    Component m("model", new RooWorkspace);
    ASSERT_EQ( m.channels().size(), 0 );
    ASSERT_EQ( m.samples().size(), 0 );
    TH1D h("h","h",2,0,2);h.Sumw2();
    h.SetBinContent(1,7);h.SetBinContent(2,4);h.SetBinError(1,0.2);

    // this line creates a sample in a channel inside the model
    m.channel("chan1").sample("samp1").SetContent(&h);
    ASSERT_EQ( m.channels().size(), 1);
    ASSERT_EQ(m.channel("chan1").samples().size(), 1);
    ASSERT_EQ( m.channel("chan1").sample("samp1").GetBinContent(1), 7 );

    // there is are observables: the channelCat and 'xaxis' observable
    ASSERT_EQ( m.obs()->size(), 2);

    // create a scale factor
    m.channel("chan1").sample("samp1").factor("f1").SetBinContent(0, 1.2, "alpha");

    ASSERT_NE( m.ws()->var("alpha"), nullptr );

    // change alpha nuisance parameter and verify sample has been scaled
    m.ws()->var("alpha")->setVal(1);
    ASSERT_DOUBLE_EQ( m.channel("chan1").sample("samp1").GetBinContent(1), 7*1.2 );

}

TEST(Component,factors) {

    Component m("model", new RooWorkspace);
    auto s = m.channel("chan1").sample("samp1");
    s.SetContent(new TH1D("h","h",1,0,1));

    // check there's only 1 factor
    ASSERT_EQ( s.factors().size(), 1 );
    s.varFactor("mu").SetBinContent(0,1);
    ASSERT_EQ( s.factors().size(), 2 );
    // first factor should still be the main histfunc
    ASSERT_STREQ( s.factors()[0].GetName(), "model_chan1_samp1_nominal");
    // add a parFactor
    s.parFactor("sf").SetBinContent(1,1);
    // first factor should still be the main histfunc
    ASSERT_EQ( s.factors().size(), 3 );
    ASSERT_STREQ( s.factors()[0].GetName(), "model_chan1_samp1_nominal");

    // can also add factors straight onto a sample, but will create a FlexibleInterpVar as the value
    // this will prevent you subsequently setting contents in bin>0
    ASSERT_TRUE( m.channel("chan1").sample("samp2").parFactor("fac").SetBinContent(1,5) );
    ASSERT_TRUE( m.channel("chan1").sample("samp2").SetBinContent(0,2)); // because FlexibleInterpVar is main comp
    ASSERT_DOUBLE_EQ( m.channel("chan1").sample("samp2").GetBinContent(1), 5*2);

}

// Tests the various ways of modifying a sample:
//   NormFactor: a simple global scaling factor
//   OverallSys: a FlexibleInterpVar normFactor with constrained np
//   ShapeFactor: a bin-by-bin set of scaling factors (ParamHistFunc)
//   ShapeSys: a constrained ShapeFactor
TEST(Component, modifiers) {

    auto w = Component::currentSpace = new RooWorkspace();

    Component m("model");
    TH1D h("h","h",2,0,4);h.Sumw2();
    h.SetBinContent(1,7);h.SetBinContent(2,4);
    auto s = m.channel("chan1").sample("samp1");
    s.SetContent(&h);


    // 1. NormFactor
    s.varFactor("normFactor").SetBinContent(0,2);
    ASSERT_DOUBLE_EQ( s.GetBinContent(1), 14);

    // 2. OverallSys
    // TODO: creating a default flexibleinterpvar when setting bin content 0 on sample
    s.parFactor("overallSys").SetBinContent(0, 5);
    s.parFactor("overallSys").SetBinContent(0, 6, "alpha"); // systematic variation
    ASSERT_TRUE( s.var("alpha") );
    ASSERT_DOUBLE_EQ( s.GetBinContent(1), 2*5*7);
    s.var("alpha")->setVal(1);
    ASSERT_DOUBLE_EQ( s.GetBinContent(1), 2*6*7);


    // 3. ShapeFactor
    ASSERT_EQ( s.varFactor("shapeFactor").factors(1).size(), 0); // no bin factors
    s.varFactor("shapeFactor").SetBinContent(1,3);
    ASSERT_EQ( s.varFactor("shapeFactor").factors(1).size(), 1); // bin factor created
    ASSERT_EQ( s.varFactor("shapeFactor").factors(1)[0].GetBinContent(0), 3);
    ASSERT_DOUBLE_EQ( s.GetBinContent(1), 2*6*7*3);

    // 4. ShapeSys - specify an error for a ShapeFactor and a constraint will be created
    // Errors by default are poisson, you can change by specifying varPar = sigma for gaussian
    ASSERT_FALSE( s.varFactor("shapeFactor").factors(1)[0].getConstraint() ); // no constraint
    s.varFactor("shapeFactor").SetBinError(1,0.1);
    ASSERT_TRUE( s.varFactor("shapeFactor").factors(1)[0].getConstraint() ); // now its constrained
    // can remove the constraint by applying an infinite error
    s.varFactor("shapeFactor").SetBinError(1,std::numeric_limits<double>::infinity());
    ASSERT_FALSE( s.varFactor("shapeFactor").factors(1)[0].getConstraint() );

    s.ws()->Print();


    // 5. statFactor: a Special ShapeSys
    s.SetBinError(1,0.5,"stat");
    //s.ws()->Print();
    ASSERT_EQ( s.varFactor("stat").factors(1).size(), 1 );
    ASSERT_EQ( s.varFactor("stat").factors(2).size(), 0 ); // second bin has no factor in it though
    ASSERT_DOUBLE_EQ( s.varFactor("stat").factors(1)[0].GetBinContent(0), 1);
    ASSERT_DOUBLE_EQ( s.varFactor("stat").factors(1)[0].constraint().args().at(0).GetBinContent(0), pow(7./0.5,2 ));

}

TEST(Component, modifiers2) {

    // try applying a shapeSys (constrained binwise varFactor) to a whole channel
    Component m("model");
    m.channel("chan1").sample("samp1").SetContent(new TH1D("h","h",2,0,2));
    m.channel("chan1").varFactor("shapeSys").SetBinError(1,0.5,"sigma"); // gaussian constraint?

    ASSERT_DOUBLE_EQ( m.channel("chan1").factor("shapeSys").factors(1).size(), 1);
    ASSERT_DOUBLE_EQ( m.channel("chan1").factor("shapeSys").factors(1)[0].GetBinContent(0), 1);
    ASSERT_TRUE( m.channel("chan1").factor("shapeSys").factors(1)[0].getConstraint());
    ASSERT_DOUBLE_EQ( m.globs()->getRealValue(Form("gobs_%s",m.channel("chan1").factor("shapeSys").factors(1)[0].GetName())),1);


    m.ws()->Print();

}


TEST(Component, nllTest) {
    auto w= new RooWorkspace;
    Component m("simPdf",w);
    TH1D h("h","h",2,0,2);h.SetBinContent(1,7);h.SetBinContent(2,4);
    m.channel("SR").sample("bkg").SetContent(&h);

    m.channel("SR").data("data").SetBinContent(1,5);
    m.channel("SR").data("data").SetBinContent(2,6);

    //m.Draw();m.data("data").Draw("same");
    auto fc = new Fitting::FitConfig(Fitting::defaultConfig());
    fc->returnBinnedNLL = true;

    ASSERT_DOUBLE_EQ( m.fitTo(m.data("data"),fc)->minNll(), -log(ROOT::Math::poisson_pdf(5,7)) -log(ROOT::Math::poisson_pdf(6,4)) );

    delete w;

}